#[macro_use]
extern crate afl;

// Fuzz all the Source files that can be given to the application by the user.
fn main() {
    fuzz_lexer_source();
}

#[allow(dead_code)]
fn fuzz_lexer_source() {
    fuzz!(|data: &[u8]| {
        if let Ok(source) = std::str::from_utf8(data) {
            let (_tree, _diagnostics) =
                df_ls_lexical_analysis::tokenize_df_raw_file(source.to_owned(), false);
        }
    });
}
