mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

#[test]
fn test_missing_newline() {
    let source = "creature_domestic[OBJECT:CREATURE][CREATURE:DOG]";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_newline".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 0,
                character: 17,
            },
            end: Position {
                line: 0,
                character: 17,
            },
        }],
    );
}
