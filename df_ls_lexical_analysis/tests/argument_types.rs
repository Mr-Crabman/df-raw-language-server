use pretty_assertions::assert_eq;

#[test]
fn test_argument_types_1() {
    let source = "header
    [REF:REF]
    [REF:'c'] char
    [REF:string]
    [REF:9] int
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
}

#[test]
fn test_argument_types_2() {
    let source = "header
    [REF:REF:REF]
    [REF:'c':'p'] char
    [REF:string:string2]
    [REF:9:-6] int
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
}

#[test]
fn test_argument_types_3() {
    let source = "header
    [REF:REF:REF:OTHER]
    [REF:'c':'p':'&'] char
    [REF:string:string2:large String]
    [REF:9:-6:93315] int
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
      (:)
      (token_value_reference)
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
      (:)
      (token_value_character)
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
      (:)
      (token_value_string)
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
      (:)
      (token_value_integer)
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
}
