mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

/// Test Windows encoded newline files.
/// instead of `\n` (LF) they have `\r\n` (CRLF)
/// For more info see: https://en.wikipedia.org/wiki/Newline#Representation
#[test]
fn test_crlf() {
    let source = "creature_domestic\r
    \r
    [OBJECT:CREATURE]\r
    \r
    [CREATURE:DOG]\r
        [CASTE:FEMALE]\r
            [FEMALE]\r
        [CASTE:MALE]\r
            [MALE]\r
        [SELECT_CASTE:ALL]\r
            [NATURAL]\r
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
}

#[test]
fn test_crlf_error_case() {
    let source = "creature_ocean_new\r
    \r
    [CREATURE:CRAB_MAN]\r
    \r
    Missing start bracket\r
    AMPHIBIOUS]\r
    \r
    Missing end bracket\r
    [COPY_TAGS_FROM:CRAB\r
    \r
    Missing first part of token\r
    [:A DF Raw Language Server developer with the head and pincers of a crab.]\r
    \r
    Missing token\r
    []\r
    \r
    First part of token has to be of type 'token_value_reference'\r
    [crab man:crab men:crab man]\r
    [100]\r
    ['c']\r
    \r
    Should not be able to spread tokens across multiple lines (probably)\r
    THIS HAS NOT BEEN IMPLEMENTED YET, SO RIGHT NOW IT'S JUST SEEN AS A NORMAL TOKEN\r
    [\r
        DESCRIPTION\r
        :This and above DESCRIPTION should be considered comments.\r
    ]\r
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "unexpected_end_bracket".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_characters".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 5,
                    character: 14,
                },
                end: Position {
                    line: 5,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 8,
                    character: 24,
                },
                end: Position {
                    line: 8,
                    character: 24,
                },
            },
            Range {
                start: Position {
                    line: 11,
                    character: 5,
                },
                end: Position {
                    line: 11,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 14,
                    character: 5,
                },
                end: Position {
                    line: 14,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 17,
                    character: 5,
                },
                end: Position {
                    line: 17,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 17,
                    character: 5,
                },
                end: Position {
                    line: 17,
                    character: 31,
                },
            },
            Range {
                start: Position {
                    line: 18,
                    character: 5,
                },
                end: Position {
                    line: 18,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 18,
                    character: 5,
                },
                end: Position {
                    line: 18,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 19,
                    character: 5,
                },
                end: Position {
                    line: 19,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 19,
                    character: 5,
                },
                end: Position {
                    line: 19,
                    character: 8,
                },
            },
            Range {
                start: Position {
                    line: 23,
                    character: 5,
                },
                end: Position {
                    line: 23,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 23,
                    character: 5,
                },
                end: Position {
                    line: 23,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 26,
                    character: 4,
                },
                end: Position {
                    line: 26,
                    character: 5,
                },
            },
        ],
    );
}
