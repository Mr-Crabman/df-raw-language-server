mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

#[test]
fn message_missing_header() {
    let source = "
    
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (comment)
  (EOF)
)
"
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("missing_header".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected file header on first line.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_missing_end_bracket() {
    let source = "test
    [REF
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 1,
                    character: 8,
                },
                end: Position {
                    line: 1,
                    character: 8,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("missing_end_bracket".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected `]`.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_unexpected_end_bracket() {
    let source = "test
    REF]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (comment)
  (EOF)
)
"
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 1,
                    character: 7,
                },
                end: Position {
                    line: 1,
                    character: 8,
                },
            },
            severity: Some(DiagnosticSeverity::Warning),
            code: Some(NumberOrString::String("unexpected_end_bracket".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Unexpected `]`. Did you forget a start bracket?".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_missing_token_name() {
    let source = "test
    []
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 1,
                    character: 5,
                },
                end: Position {
                    line: 1,
                    character: 5,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("missing_token_name".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "The token name is missing.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_unexpected_characters() {
    let source = "test
    [preREF]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 1,
                    character: 5,
                },
                end: Position {
                    line: 1,
                    character: 8,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("unexpected_characters".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Unexpected characters found.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_missing_newline() {
    let source = "test[REF]";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (EOF)
)
"
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 0,
                    character: 4,
                },
                end: Position {
                    line: 0,
                    character: 4,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("missing_newline".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected a newline here.".to_owned(),
            ..Default::default()
        }]
    );
}
