use pretty_assertions::assert_eq;

#[test]
fn test_basic_grammar() {
    let source = "creature_domestic

    [OBJECT:CREATURE]
    
    [CREATURE:DOG]
        [CASTE:FEMALE]
            [FEMALE]
        [CASTE:MALE]
            [MALE]
        [SELECT_CASTE:ALL]
            [NATURAL]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
}
