mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

#[test]
fn test_end_of_file_1() {
    let source = "";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
}

#[test]
fn test_end_of_file_1_1() {
    let source = "  ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 0,
                character: 2,
            },
            end: Position {
                line: 0,
                character: 2,
            },
        }],
    );
}

#[test]
fn test_end_of_file_1_2() {
    let source = "  \n";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 0,
                character: 2,
            },
            end: Position {
                line: 0,
                character: 2,
            },
        }],
    );
}

#[test]
fn test_end_of_file_2() {
    let source = "descriptor_color_standard
";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (EOF)
)
"
    );
}

#[test]
fn test_end_of_file_2_1() {
    let source = "descriptor_color_standard";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (EOF)
)
"
    );
}

#[test]
fn test_end_of_file_3() {
    let source = "descriptor_color_standard
    
    [";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_end_of_file_4() {
    let source = "descriptor_color_standard
    
    [TEST";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        }],
    );
}

#[test]
fn test_end_of_file_5() {
    let source = "descriptor_color_standard
    
    [TEST:";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_empty)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 10,
            },
            end: Position {
                line: 2,
                character: 10,
            },
        }],
    );
}

#[test]
fn test_end_of_file_6() {
    let source = "descriptor_color_standard
    
    [TEST:string";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 16,
            },
            end: Position {
                line: 2,
                character: 16,
            },
        }],
    );
}

#[test]
fn test_end_of_file_7() {
    let source = "descriptor_color_standard
    
    [TEST::";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_empty)
      (:)
      (token_value_empty)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 11,
            },
            end: Position {
                line: 2,
                character: 11,
            },
        }],
    );
}

#[test]
fn test_end_of_file_8() {
    let source = "descriptor_color_standard
    
    [TEST::str";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_empty)
      (:)
      (token_value_string)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 14,
            },
            end: Position {
                line: 2,
                character: 14,
            },
        }],
    );
}

#[test]
fn test_end_of_file_9() {
    let source = "descriptor_color_standard
    
    [TEST::REF";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_empty)
      (:)
      (token_value_reference)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 14,
            },
            end: Position {
                line: 2,
                character: 14,
            },
        }],
    );
}

#[test]
fn test_end_of_file_10() {
    let source = "descriptor_color_standard
    [TEST:REF]";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}
