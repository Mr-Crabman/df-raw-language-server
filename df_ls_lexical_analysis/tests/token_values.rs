use pretty_assertions::assert_eq;

#[test]
fn test_token_values() {
    let source = "descriptor_color_standard

    [NAME:Amber] string
    [NAME:A mber] string
    [NAME:AMBER] ref
    [NAME:AMBER_] ref
    [NAME:AMBER ] string
    [NAME:AMBER-] string
    [NAME:AMBER:] ref + empty arg
    [NAME:AMBER   ] string
    [NAME:AMBER   a] string
    [NAME:AMBER   A] string
    [NAME:AMBER___A] ref
    [NAME:AMbER___A] string
    [NAME:AMbER] string
    [NAME:9AMbER] string
    [NAME:9AMBER] ref
    [NAME:9AMBER ] string
    [NAME:9AMB2ER] ref
    [NAME:AMB2ER] ref
    [NAME:52] int
    [NAME:-52] int
    [NAME:42] int
    [NAME:-42] int
    [NAME:2556888] int
    [NAME:000] int
    [NAME:0] int
    [NAME:'c'] char
    [NAME:'\\n'] string
    [NAME:'k'] char
    [NAME:'R'] char
    [NAME:'9'] char
    [NAME:'7'] char
    [NAME:':'] char
    [NAME:'*'] char
    [NAME:'''] char
    [NAME:'\"'] char
    [NAME:\"\"\"] string
    [NAME:'@'] char
    [NAME:POP] ref
    [NAME:POP9] ref
    [NAME:99P9] ref
    [NAME:99 P9] string
    [NAME:9p P9] string
    [NAME:9p_P9] string
    [NAME:9P_P9] ref
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
      (:)
      (token_value_empty)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_integer)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_character)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
}
