use super::*;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use regex::Regex;
use std::cmp::Ordering;
use std::rc::Rc;

#[derive(Clone, Debug)]
pub(crate) struct TokenizerHelper {
    source: String,
    index: usize,
    line_nr: usize,
    column_nr: usize,
    id_counter: u64,
    tree: Rc<Tree>,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum TokenMatchStatus {
    Ok(DataNode),
    OkWithPrefixFound(DataNode, DataNode),
    EoF,
    NoMatch,
}

impl TokenizerHelper {
    pub fn new(source: String, tree: &Rc<Tree>) -> Self {
        Self {
            source,
            index: 0,
            line_nr: 0,
            column_nr: 0,
            id_counter: 0,
            tree: tree.clone(),
        }
    }

    pub fn check_if_next_char_match(&self, character: char) -> bool {
        let source_bytes = self.source.as_bytes();
        if self.index >= source_bytes.len() {
            return false;
        }
        let found_char = source_bytes[self.index];
        found_char == (character as u8)
    }

    pub fn get_next_match(
        &mut self,
        regex: &Regex,
        kind: &str,
        name: Option<&str>,
        optional: bool,
        move_cursor: bool,
    ) -> TokenMatchStatus {
        // check if string is not empty
        if self.index >= self.source.len() {
            return TokenMatchStatus::EoF;
        }
        // Look first match
        let (_done, to_match) = self.source.split_at(self.index);
        if let Some(mat) = regex.find(to_match) {
            let start_byte = self.index + mat.start();
            let start_point = self.get_moved_point(start_byte).unwrap();
            let mut prefix_found = false;
            // Get all text before found match
            let (prefix, _) = to_match.split_at(mat.start());
            if !prefix.is_empty() {
                if optional {
                    return TokenMatchStatus::NoMatch;
                }
                // Check if prefix is across multiple lines.
                // State no match do avoid over extending token
                if prefix.contains('\n') {
                    return TokenMatchStatus::NoMatch;
                }
                error!("Found string before match in ({}): {}", kind, prefix);
                prefix_found = true;
            }
            // Calculate new index
            let new_index = self.index + mat.end();
            let end_byte = new_index;
            // Create prefix node
            let prefix_found = match prefix_found {
                true => {
                    let prefix_start_byte = self.index;
                    let prefix_end_byte = self.index + mat.start();
                    let mut prefix_node = self.create_tsnode(
                        prefix_start_byte,
                        self.get_point(),
                        prefix_end_byte,
                        self.get_moved_point(prefix_end_byte).unwrap(),
                        "ERROR",
                        Some("ERROR"),
                    );
                    prefix_node.kind_id = u16::MAX;
                    Some(prefix_node)
                }
                false => None,
            };
            // Get point and maybe move cursor
            let end_point = if move_cursor {
                self.move_index(new_index)
            } else {
                self.get_moved_point(new_index).unwrap()
            };
            let node = self.create_tsnode(start_byte, start_point, end_byte, end_point, kind, name);
            match prefix_found {
                Some(prefix) => TokenMatchStatus::OkWithPrefixFound(prefix, node),
                None => TokenMatchStatus::Ok(node),
            }
        } else {
            TokenMatchStatus::NoMatch
        }
    }

    pub fn create_tsnode(
        &mut self,
        start_byte: usize,
        start_point: Point,
        end_byte: usize,
        end_point: Point,
        kind: &str,
        name: Option<&str>,
    ) -> DataNode {
        let name = match name {
            Some(value) => Some(value.to_owned()),
            None => None,
        };
        self.id_counter += 1;
        DataNode {
            id: self.id_counter,
            kind_id: 0,
            kind: kind.to_owned(),
            name,
            start_byte,
            end_byte,
            start_point,
            end_point,

            children_ids: vec![],
            parent_id: None,
            next_sibling_id: None,
            prev_sibling_id: None,
            tree: Rc::downgrade(&self.tree),
        }
    }

    pub fn create_start_tsnode(&mut self, kind: &str, name: Option<&str>) -> DataNode {
        let name = match name {
            Some(value) => Some(value.to_owned()),
            None => None,
        };
        self.id_counter += 1;
        DataNode {
            id: self.id_counter,
            kind_id: 0,
            kind: kind.to_owned(),
            name,
            start_byte: self.index,
            end_byte: self.index,
            start_point: self.get_point(),
            end_point: self.get_point(),

            children_ids: vec![],
            parent_id: None,
            next_sibling_id: None,
            prev_sibling_id: None,
            tree: Rc::downgrade(&self.tree),
        }
    }

    pub fn set_end_point_for(&self, k: u64) {
        if let Some(mut node) = self.tree.get_tsnode(k) {
            node.end_byte = self.index;
            node.end_point = self.get_point();
            self.tree.update_node(k, node);
        }
    }

    pub fn add_node_to_tree(&self, node: DataNode, parent_id: u64) {
        let node_id = node.id;
        self.tree.add_tsnode(node);
        self.tree.add_child_to_node(parent_id, node_id);
    }

    pub fn get_point(&self) -> Point {
        Point {
            row: self.line_nr,
            column: self.column_nr,
        }
    }

    pub fn move_index(&mut self, new_index: usize) -> Point {
        let new_point = self.get_moved_point(new_index).unwrap();
        self.line_nr = new_point.row;
        self.column_nr = new_point.column;
        self.index = new_index;
        new_point
    }

    pub fn direct_mode_index_and_point(&mut self, new_index: usize, new_point: Point) {
        self.line_nr = new_point.row;
        self.column_nr = new_point.column;
        self.index = new_index;
    }

    fn get_moved_point(&self, new_index: usize) -> Result<Point, ()> {
        match new_index.cmp(&self.index) {
            Ordering::Greater => {}
            Ordering::Less => {
                error!(
                    "Can not move index backwards: old:{}, new:{}",
                    self.index, new_index
                );
                return Err(());
            }
            Ordering::Equal => {
                return Ok(self.get_point());
            }
        }
        let (_done, to_match) = self.source.split_at(self.index);
        let (current, _future) = to_match.split_at(new_index - self.index);
        // debug!("Move past: {}", current);

        // Count amount of newlines
        let amount_of_newlines = current.matches('\n').count();
        let new_line_nr = self.line_nr + amount_of_newlines;

        // Count char since last newline
        let new_column_nr = if let Some(last_newline_index) = current.rfind('\n') {
            current.len() - last_newline_index - 1
        } else {
            // no newline parsed, add amount of chars
            self.column_nr + current.len()
        };
        // debug!(
        //     "Cursor pos: {} - {}:{}",
        //     self.index, self.line_nr, self.column_nr
        // );
        Ok(Point {
            row: new_line_nr,
            column: new_column_nr,
        })
    }
}
