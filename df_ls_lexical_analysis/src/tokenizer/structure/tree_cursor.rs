use super::Node;

#[derive(Clone, Debug, PartialEq, Hash)]
pub struct TreeCursor {
    cursor: Node,
}

impl TreeCursor {
    /// Create new TreeCursor.
    pub fn new(cursor: Node) -> TreeCursor {
        TreeCursor { cursor }
    }

    /// Get the tree cursor's current [Node].
    pub fn node(&self) -> Node {
        self.cursor.clone()
    }

    /// Move this cursor to the first child of its current node.
    ///
    /// This returns `true` if the cursor successfully moved, and returns `false`
    /// if there were no children.
    pub fn goto_first_child(&mut self) -> bool {
        if let Some(next) = self.cursor.child(0) {
            self.cursor = next;
            true
        } else {
            false
        }
    }

    /// Move this cursor to the parent of its current node.
    ///
    /// This returns `true` if the cursor successfully moved, and returns `false`
    /// if there was no parent node (the cursor was already on the root node).
    pub fn goto_parent(&mut self) -> bool {
        if let Some(next) = self.cursor.parent() {
            self.cursor = next;
            true
        } else {
            false
        }
    }

    /// Move this cursor to the next sibling of its current node.
    ///
    /// This returns `true` if the cursor successfully moved, and returns `false`
    /// if there was no next sibling node.
    pub fn goto_next_sibling(&mut self) -> bool {
        if let Some(next) = self.cursor.next_sibling() {
            self.cursor = next;
            true
        } else {
            false
        }
    }

    /// Re-initialize this tree cursor to start at a different node.
    pub fn reset(&mut self, node: Node) {
        self.cursor = node;
    }
}
