use super::*;
use df_ls_diagnostics::lsp_types::Diagnostic;
use df_ls_diagnostics::{DMExtraInfo, DiagnosticMessageSet, DiagnosticsInfo, Position, Range};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use std::collections::HashMap;
use std::rc::Rc;

pub fn tokenize_df_raw_file(
    source: String,
    load_diagnostic_messages: bool,
) -> (Rc<Tree>, Vec<Diagnostic>) {
    let mut diagnostic_info = if load_diagnostic_messages {
        DiagnosticsInfo::load_from_file(
            DiagnosticMessageSet::Lexer,
            Some("DF RAW Language Server".to_owned()),
        )
    } else {
        DiagnosticsInfo {
            diagnostics: vec![],
            diagnostics_messages: HashMap::new(),
            source: Some("DF RAW Language Server".to_owned()),
        }
    };

    let tree = Rc::new(Tree::default());
    tree.add_tsnode(DataNode {
        id: ROOT_ID,
        kind_id: 0,
        kind: "raw_file".to_owned(),
        name: Some("raw_file".to_owned()),
        start_byte: 0,
        end_byte: source.len(),
        start_point: Point { row: 0, column: 0 },
        end_point: Point { row: 0, column: 0 },

        children_ids: vec![],
        parent_id: None,
        next_sibling_id: None,
        prev_sibling_id: None,
        tree: Rc::downgrade(&tree),
    });
    let mut tok_help = TokenizerHelper::new(source, &tree);
    let regex_list = RegexList::new();

    tokenize_content(&mut tok_help, &regex_list, &mut diagnostic_info);

    // Add EOF (End of File) token
    let eof_token = tok_help.create_start_tsnode("EOF", Some("EOF"));
    tok_help.add_node_to_tree(eof_token, ROOT_ID);

    // Update root
    let mut root = tree.get_tsnode(ROOT_ID).unwrap();
    root.end_point = tok_help.get_point();
    tree.update_node(ROOT_ID, root);

    tree.finalize_tree();

    (tree, diagnostic_info.diagnostics)
}

fn tokenize_content(
    mut tok_help: &mut TokenizerHelper,
    regex_list: &RegexList,
    mut diagnostics: &mut DiagnosticsInfo,
) {
    let _optional_line_space =
        tok_help.get_next_match(&regex_list.line_space, "", None, true, true);

    let header = tok_help.get_next_match(&regex_list.header, "header", Some("header"), false, true);
    match header {
        TokenMatchStatus::Ok(result) => {
            tok_help.add_node_to_tree(result, ROOT_ID);
        }
        TokenMatchStatus::OkWithPrefixFound(prefix, result) => {
            handel_prefix(prefix, &mut diagnostics);
            tok_help.add_node_to_tree(result, ROOT_ID);
        }
        _ => {
            diagnostics.add_message(
                DMExtraInfo::new(Range {
                    start: Position {
                        line: tok_help.get_point().row as u32,
                        character: tok_help.get_point().column as u32,
                    },
                    end: Position {
                        line: tok_help.get_point().row as u32,
                        character: tok_help.get_point().column as u32,
                    },
                }),
                "missing_header",
            );
        }
    }

    let newline = tok_help.get_next_match(&regex_list.newline, "", None, false, true);
    match newline {
        TokenMatchStatus::Ok(_result) => {}
        TokenMatchStatus::OkWithPrefixFound(prefix, _result) => {
            handel_prefix(prefix, &mut diagnostics);
        }
        TokenMatchStatus::EoF => {
            // This is valid, no error needed
            return;
        }
        _ => {
            diagnostics.add_message(
                // No extra template data needed
                DMExtraInfo::new(Range {
                    start: Position {
                        line: tok_help.get_point().row as u32,
                        character: tok_help.get_point().column as u32,
                    },
                    end: Position {
                        line: tok_help.get_point().row as u32,
                        character: tok_help.get_point().column as u32,
                    },
                }),
                "missing_newline",
            );
        }
    }

    loop {
        if !(tok_help.check_if_next_char_match(']') || tok_help.check_if_next_char_match('[')) {
            // comment
            let comment = tok_help.get_next_match(
                &regex_list.comment,
                "comment",
                Some("comment"),
                true,
                true,
            );
            match comment {
                TokenMatchStatus::Ok(result) => {
                    tok_help.add_node_to_tree(result, ROOT_ID);
                }
                TokenMatchStatus::OkWithPrefixFound(prefix, result) => {
                    debug_assert!(false, "This should be unreachable with current regex.");
                    handel_prefix(prefix, &mut diagnostics);
                    tok_help.add_node_to_tree(result, ROOT_ID);
                }
                TokenMatchStatus::EoF => break,
                _ => {}
            }
        }

        // Look for if there is a separated `]` somewhere.
        if tok_help.check_if_next_char_match(']') {
            let unexpected_close_token = tok_help.get_next_match(
                &regex_list.token_close_bracket,
                "ERROR",
                Some("ERROR"),
                true,
                true,
            );
            match unexpected_close_token {
                TokenMatchStatus::Ok(result) => {
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(result.get_range()),
                        "unexpected_end_bracket",
                    );
                    // Look for comment again
                    continue;
                }
                TokenMatchStatus::EoF => break,
                _ => {}
            }
        }
        // start general_token
        let general_token = tok_help.create_start_tsnode("general_token", Some("general_token"));
        let general_token_id = general_token.id;
        // This token only need to be added if it is not empty
        // `[`
        let open_bracket =
            tok_help.get_next_match(&regex_list.token_open_bracket, "[", None, false, true);
        match open_bracket {
            TokenMatchStatus::Ok(result) => {
                // Add token because it is not empty
                tok_help.add_node_to_tree(general_token.clone(), ROOT_ID);
                tok_help.add_node_to_tree(result, general_token_id);
            }
            TokenMatchStatus::OkWithPrefixFound(prefix, result) => {
                debug_assert!(false, "This should be unreachable.");
                // Add token because it is not empty
                tok_help.add_node_to_tree(general_token.clone(), ROOT_ID);
                handel_prefix(prefix, &mut diagnostics);
                tok_help.add_node_to_tree(result, general_token_id);
            }
            TokenMatchStatus::EoF => break,
            _ => {}
        }

        // If next token is `:` or `]` this means the `token_value_reference` is missing
        if tok_help.check_if_next_char_match(':') || tok_help.check_if_next_char_match(']') {
            // Add missing token
            let token_ref = tok_help
                .create_start_tsnode("token_value_reference", Some("token_value_reference"));
            diagnostics.add_message(
                // No extra template data needed
                DMExtraInfo::new(token_ref.get_range()),
                "missing_token_name",
            );
            tok_help.add_node_to_tree(token_ref, general_token_id);
        } else {
            // `token_value_reference`
            let token_ref = tok_help.get_next_match(
                &regex_list.token_value_reference,
                "token_value_reference",
                Some("token_value_reference"),
                false,
                true,
            );
            match token_ref {
                TokenMatchStatus::Ok(result) => {
                    tok_help.add_node_to_tree(result, general_token_id);
                }
                TokenMatchStatus::OkWithPrefixFound(prefix, result) => {
                    handel_prefix(prefix, &mut diagnostics);
                    tok_help.add_node_to_tree(result, general_token_id);
                }
                TokenMatchStatus::NoMatch => {
                    // Add missing token
                    let token_ref = tok_help.create_start_tsnode(
                        "token_value_reference",
                        Some("token_value_reference"),
                    );
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(token_ref.get_range()),
                        "missing_token_name",
                    );
                    tok_help.add_node_to_tree(token_ref, general_token_id);
                }
                _ => {
                    // Add missing `token_value_reference`
                    let token_ref = tok_help.create_start_tsnode(
                        "token_value_reference",
                        Some("token_value_reference"),
                    );
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(token_ref.get_range()),
                        "missing_token_name",
                    );
                    tok_help.add_node_to_tree(token_ref, general_token_id);
                    // Add missing `]`
                    let close_bracket = tok_help.create_start_tsnode("]", None);
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(close_bracket.get_range()),
                        "missing_end_bracket",
                    );
                    tok_help.add_node_to_tree(close_bracket, general_token_id);
                    // Close `general_token`
                    tok_help.set_end_point_for(general_token_id);
                    return;
                }
            }
        }

        // start general_token_values
        let general_token_values =
            tok_help.create_start_tsnode("general_token_values", Some("general_token_values"));
        let general_token_values_id = general_token_values.id;
        // This token only need to be added if it is not empty
        let mut general_token_values_added = false;
        loop {
            // See if early return is wanted
            if tok_help.check_if_next_char_match(']')
                || tok_help.check_if_next_char_match('\n')
                || tok_help.check_if_next_char_match('\r')
            {
                break;
            }
            // `:`
            let separator =
                tok_help.get_next_match(&regex_list.token_separator, ":", None, true, true);
            match separator {
                TokenMatchStatus::Ok(result) => {
                    if !general_token_values_added {
                        // Add token because it is not empty
                        general_token_values_added = true;
                        tok_help.add_node_to_tree(general_token_values.clone(), general_token_id);
                    }
                    tok_help.add_node_to_tree(result, general_token_values_id);
                }
                TokenMatchStatus::OkWithPrefixFound(_prefix, _result) => {
                    unreachable!("Token is optional");
                }
                TokenMatchStatus::NoMatch => break, // go out of `general_token_values`
                TokenMatchStatus::EoF => {
                    // Close `general_token_values_added`
                    if general_token_values_added {
                        tok_help.set_end_point_for(general_token_values_id);
                    }
                    // Add missing token
                    let close_bracket = tok_help.create_start_tsnode("]", None);
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(close_bracket.get_range()),
                        "missing_end_bracket",
                    );
                    tok_help.add_node_to_tree(close_bracket, general_token_id);
                    // Close `general_token`
                    tok_help.set_end_point_for(general_token_id);
                    return;
                }
            }
            // See if early return is wanted
            if tok_help.check_if_next_char_match(']')
                || tok_help.check_if_next_char_match('[')
                || tok_help.check_if_next_char_match('\n')
                || tok_help.check_if_next_char_match('\r')
            {
                // Add empty token value node.
                let token_value_empty =
                    tok_help.create_start_tsnode("token_value_empty", Some("token_value_empty"));
                tok_help.add_node_to_tree(token_value_empty, general_token_values_id);
                break;
            }
            // check if empty argument
            if tok_help.check_if_next_char_match(':') {
                // Add empty token value node.
                let token_value_empty =
                    tok_help.create_start_tsnode("token_value_empty", Some("token_value_empty"));
                tok_help.add_node_to_tree(token_value_empty, general_token_values_id);
                continue;
            }

            let found_token = tokenize_token_value(&mut tok_help, &regex_list, &mut diagnostics);
            if let Some(found_token) = found_token {
                tok_help.add_node_to_tree(found_token, general_token_values_id);
            } else {
                // Add empty token value node.
                let token_value_empty =
                    tok_help.create_start_tsnode("token_value_empty", Some("token_value_empty"));
                tok_help.add_node_to_tree(token_value_empty, general_token_values_id);
                // In case of EOF (or no match)
                tok_help.set_end_point_for(general_token_values_id);
                // Add missing token
                let close_bracket = tok_help.create_start_tsnode("]", None);
                diagnostics.add_message(
                    // No extra template data needed
                    DMExtraInfo::new(close_bracket.get_range()),
                    "missing_end_bracket",
                );
                tok_help.add_node_to_tree(close_bracket, general_token_id);
                // Close `general_token`
                tok_help.set_end_point_for(general_token_id);
                return;
            }
        }
        // end general_token_values
        tok_help.set_end_point_for(general_token_values_id);

        // If next token is ` `, `\n` or `\r` this means the `close_bracket` is missing
        if tok_help.check_if_next_char_match(' ')
            || tok_help.check_if_next_char_match('[')
            || tok_help.check_if_next_char_match('\n')
            || tok_help.check_if_next_char_match('\r')
        {
            // Add missing token
            let close_bracket = tok_help.create_start_tsnode("]", None);
            diagnostics.add_message(
                // No extra template data needed
                DMExtraInfo::new(close_bracket.get_range()),
                "missing_end_bracket",
            );
            tok_help.add_node_to_tree(close_bracket, general_token_id);
        } else {
            // `]`
            let close_bracket =
                tok_help.get_next_match(&regex_list.token_close_bracket, "]", None, false, true);
            match close_bracket {
                TokenMatchStatus::Ok(result) => {
                    tok_help.add_node_to_tree(result, general_token_id);
                }
                TokenMatchStatus::OkWithPrefixFound(prefix, result) => {
                    handel_prefix(prefix, &mut diagnostics);
                    tok_help.add_node_to_tree(result, general_token_id);
                }
                TokenMatchStatus::NoMatch => {
                    // Add missing token
                    let close_bracket = tok_help.create_start_tsnode("]", None);
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(close_bracket.get_range()),
                        "missing_end_bracket",
                    );
                    tok_help.add_node_to_tree(close_bracket, general_token_id);
                }
                _ => {
                    debug_assert!(false, "This should be unreachable.");
                    // Add missing `]`
                    let close_bracket = tok_help.create_start_tsnode("]", None);
                    diagnostics.add_message(
                        // No extra template data needed
                        DMExtraInfo::new(close_bracket.get_range()),
                        "missing_end_bracket",
                    );
                    tok_help.add_node_to_tree(close_bracket, general_token_id);
                    // Close `general_token`
                    tok_help.set_end_point_for(general_token_id);
                    return;
                }
            }
        }
        // end general_token
        tok_help.set_end_point_for(general_token_id);
    }
}

fn tokenize_token_value(
    tok_help: &mut TokenizerHelper,
    regex_list: &RegexList,
    _diagnostics: &mut DiagnosticsInfo,
) -> Option<DataNode> {
    // Test if `token_value_integer` (Does not move cursor)
    let token_int = tok_help.get_next_match(
        &regex_list.token_value_integer,
        "token_value_integer",
        Some("token_value_integer"),
        true,
        false,
    );
    let mut int_len = 0;
    match &token_int {
        TokenMatchStatus::Ok(result) => int_len = result.end_byte - result.start_byte,
        TokenMatchStatus::EoF => return None,
        _ => {}
    }
    // Test if `token_value_character` (Does not move cursor)
    let token_char = tok_help.get_next_match(
        &regex_list.token_value_character,
        "token_value_character",
        Some("token_value_character"),
        true,
        false,
    );
    let mut char_len = 0;
    match &token_char {
        TokenMatchStatus::Ok(result) => char_len = result.end_byte - result.start_byte,
        TokenMatchStatus::EoF => return None,
        _ => {}
    }
    // Test if `token_value_reference` (Does not move cursor)
    let token_ref = tok_help.get_next_match(
        &regex_list.token_value_reference,
        "token_value_reference",
        Some("token_value_reference"),
        true,
        false,
    );
    let mut ref_len = 0;
    match &token_ref {
        TokenMatchStatus::Ok(result) => ref_len = result.end_byte - result.start_byte,
        TokenMatchStatus::EoF => return None,
        _ => {}
    }
    // Test if `token_value_string` (Does not move cursor)
    let token_string = tok_help.get_next_match(
        &regex_list.token_value_string,
        "token_value_string",
        Some("token_value_string"),
        true,
        false,
    );
    let mut string_len = 0;
    match &token_string {
        TokenMatchStatus::Ok(result) => string_len = result.end_byte - result.start_byte,
        TokenMatchStatus::EoF => return None,
        _ => {}
    }

    // Get longest match and use that.
    let longest_match = *vec![int_len, char_len, ref_len, string_len]
        .iter()
        .max()
        .unwrap();

    if int_len == longest_match {
        match token_int {
            TokenMatchStatus::Ok(result) => {
                // Move cursor directly
                tok_help.direct_mode_index_and_point(result.end_byte, result.end_point);
                return Some(result);
            }
            TokenMatchStatus::EoF => return None,
            _ => {}
        }
    } else if char_len == longest_match {
        match token_char {
            TokenMatchStatus::Ok(result) => {
                // Move cursor directly
                tok_help.direct_mode_index_and_point(result.end_byte, result.end_point);
                return Some(result);
            }
            TokenMatchStatus::EoF => return None,
            _ => {}
        }
    } else if ref_len == longest_match {
        match token_ref {
            TokenMatchStatus::Ok(result) => {
                // Move cursor directly
                tok_help.direct_mode_index_and_point(result.end_byte, result.end_point);
                return Some(result);
            }
            TokenMatchStatus::EoF => return None,
            _ => {}
        }
    } else if string_len == longest_match {
        match token_string {
            TokenMatchStatus::Ok(result) => {
                // Move cursor directly
                tok_help.direct_mode_index_and_point(result.end_byte, result.end_point);
                return Some(result);
            }
            TokenMatchStatus::EoF => return None,
            _ => {}
        }
    } else {
        unreachable!("No token value was the longest");
    }

    // `OkWithPrefixFound` can never happen here because
    // all `get_next_match` here should have `optional` set to true.
    unreachable!(
        "No token value was found, but also no EOF, \
        so `[`, `]`, `:`, `\\r` or `\\n` was found. This should have ben caught earlier."
    );
}

fn handel_prefix(prefix: DataNode, diagnostics: &mut DiagnosticsInfo) {
    diagnostics.add_message(
        // No extra template data needed
        DMExtraInfo::new(prefix.get_range()),
        "unexpected_characters",
    );
}
