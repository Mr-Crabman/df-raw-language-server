# Dwarf Fortress RAW language server
[![Discord](https://img.shields.io/discord/762761192510455850?logo=discord)](https://discord.gg/6eKf5ZY)

[![pipeline status](https://gitlab.com/df-modding-tools/df-raw-language-server/badges/master/pipeline.svg)](https://gitlab.com/df-modding-tools/df-raw-language-server/-/commits/master)
[![coverage report](https://gitlab.com/df-modding-tools/df-raw-language-server/badges/master/coverage.svg)](https://codecov.io/gl/df-modding-tools/df-raw-language-server)

This repo is for a language server for [Dwarf Fortress RAW files](https://dwarffortresswiki.org/index.php/Raw_file).
These files can be used to modify (mod) the game.

This repo is work in progress and might not work in some cases.

## Syntax

If you want to take a look at the syntax this extension is based on look here:
https://gitlab.com/df-modding-tools/df-raw-syntax

### Implemented

- ✅: Implemented
- ❌: Not Implemented
- 🕗: Work in progress, partly implemented

| Token                         | Status |
| ----------------------------- | ------ |
| `[OBJECT:BODY]`               | ✅      |
| `[OBJECT:BODY_DETAIL_PLAN]`   | ❌      |
| `[OBJECT:BUILDING]`           | ❌      |
| `[OBJECT:CREATURE]`           | 🕗      |
| `[OBJECT:CREATURE_VARIATION]` | ❌      |
| `[OBJECT:DESCRIPTOR_COLOR]`   | ✅      |
| `[OBJECT:DESCRIPTOR_PATTERN]` | ✅      |
| `[OBJECT:DESCRIPTOR_SHAPE]`   | ✅      |
| `[OBJECT:ENTITY]`             | ✅      |
| `[OBJECT:GRAPHICS]`           | ❌      |
| `[OBJECT:INTERACTION]`        | ❌      |
| `[OBJECT:INORGANIC]`          | ❌      |
| `[OBJECT:ITEM]`               | 🕗      |
| `[OBJECT:LANGUAGE]`           | ✅      |
| `[OBJECT:MATERIAL_TEMPLATE]`  | ❌      |
| `[OBJECT:PLANT]`              | ❌      |
| `[OBJECT:REACTION]`           | ✅      |
| `[OBJECT:TISSUE_TEMPLATE]`    | ✅      |

## Contribute

If you want to contribute, join our [Discord](https://discord.gg/6eKf5ZY).

### Development

NOTE: Following steps are mainly for Linux. Similar steps exist for Windows and Mac OS.
To compile and use this project you need the following:

* **Rust** (rustup + cargo) ([Linux](https://www.rust-lang.org/tools/install), [Windows](https://www.rust-lang.org/tools/install?platform_override=win)) 
* **Node.js/NPM** [Node.js instructions](https://nodejs.org/en/)
* **TypeScript** [TypeScript intructions](https://www.npmjs.com/package/typescript)

Once you have the requirements follow the steps:
* Open the folder of the repo and open a terminal here.
* Build Rust source and grammar: `cargo run -- -d start`
** This should finish without any problems.
* Open project in VSCode (drag `df-raw-language-server` folder into the 'File Explorer')
* Install the [Dwarf Fortress RAW extension](https://marketplace.visualstudio.com/items?itemName=df-modding-tools.dwarf-fortress-raw) in VSCode.
* Build Language Server client
```bash
cd ./df_ls_clients/df_ls_vscode/
npm install --production=false
cd ../../
```
* Go to debug/run tab in VSCode.
* Run `Server + Client`
* A new VSCode window will open.
* Open a DF Raw File.

Run parser on debug file:
```
cargo run -- -d debug
```

#### Project structure info

This project is divided into several parts. 
Each part has its own part to play and will feed in to the next step:
- **1: Lexical Analysis**: This takes the source file and split the file into tokens. 
This step will output a `Tree` objects.
The `Tree` object is also referred to as an [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree).
The Tokenizer will also report problems in the AST. The next step will pick these errors up and
create diagnostic message from these.
- **2: Syntax Analysis**: Also known as the first pass. 
This will take in the `Tree` object and the source and use this data to create a data structure. 
During this step the more problems will be converted to diagnostic messages.
- **3: Semantic Analysis**: Also known as the second pass.
This step will take the data structure and check the structure so everything makes sense.
Some checks can only happen in this stage as it requires the full structure to be known.
This stage will return more diagnostic messages.

Then there are 2 more part to this project. These are more to create functional program.
- **DF Language server**: This is the actual Language server and will create a TCP API server 
that follows the [Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/).
- **DF LS Clients**: This is a collection of clients for the different 
[IDEs](https://en.wikipedia.org/wiki/Integrated_development_environment).
If more IDEs want to be supported this is the place to add them.

## License

This project is licensed under the MIT or Apache 2.0 license.

All contributions to this project will be similarly licensed.
