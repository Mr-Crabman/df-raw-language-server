#!/bin/sh -e

# Clean all old builds
#docker system prune -a

# General

docker login

# Code check

docker build --no-cache -t dfmoddingtools/code_check:x86_64-unknown-linux-gnu ./docker/code_check/
docker push dfmoddingtools/code_check:x86_64-unknown-linux-gnu

# Linux

docker build --no-cache -t dfmoddingtools/builder:x86_64-unknown-linux-gnu ./docker/x86_64-unknown-linux-gnu/
docker push dfmoddingtools/builder:x86_64-unknown-linux-gnu

# Windows

#docker build --no-cache -t dfmoddingtools/builder:x86_64-pc-windows-gnu ./docker/x86_64-pc-windows-gnu/
#docker push dfmoddingtools/builder:x86_64-pc-windows-gnu
