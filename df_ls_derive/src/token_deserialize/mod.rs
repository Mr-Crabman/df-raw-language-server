mod enum_value;
mod struct_de_info;
mod token_de_info;

use crate::common;
use enum_value::*;
use quote::quote;
pub use struct_de_info::*;
use syn::Ident;
pub use token_de_info::*;

pub fn impl_token_deserialize_macro(ast: &syn::DeriveInput) -> proc_macro2::TokenStream {
    let name = &ast.ident;
    let fields = common::get_struct_enum_fields(&ast);
    let struct_info = get_struct_enum_de_info(&ast);
    // If this had the `enum_value` set, use different function.
    if struct_info.enum_value {
        return impl_token_deserialize_macro_enum_value(&ast);
    }

    let parameter_checks = if struct_info.second_par_check {
        let token_name = struct_info.token.clone().unwrap();
        let match_second_par = list_match_second_par(&fields);
        let allowed_second_args = list_allowed_tokens_strings(&fields, &ast)
            .iter()
            .map(|item| format!("`{}`", item))
            .collect::<Vec<String>>()
            .join(", ");
        quote! {
            let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics).unwrap();
            match token.check_token::<Self>(&source, &mut diagnostics, false){
                Ok(_) => {},
                Err(_) => {
                    // Go back up the stack
                    if new_self == Self::default() {
                        return (LoopControl::ErrBreak, new_self);
                    }
                    return (LoopControl::Break, new_self);
                }
            };
            let arg0_val = &token.arguments.get(0).unwrap().value;
            let arg1 = &token.arguments.get(1).unwrap();
            // Check if first argument is name
            if arg0_val != &TokenValue::TVReference(#token_name.to_owned()) {
                // If nothing changed
                if new_self == Self::default() {
                    return (LoopControl::ErrBreak, new_self);
                }
                return (LoopControl::Break, new_self);
            }
            if let Err(_) = Token::consume_token(&mut cursor) {
                return (LoopControl::Break, new_self);
            }
            match &arg1.value {
                TokenValue::TVReference(value) => {
                    debug!("Matching {} in {}", value, stringify!(#name));
                    match value.as_ref() as &str {
                        #match_second_par
                        object_type => {
                            diagnostics.add_message(DMExtraInfo {
                                range: arg1.node.get_range(),
                                message_template_data: hash_map! {
                                    "found_parameter" => format!("`{}`", object_type),
                                    "token_name" => format!("`{}`", #token_name),
                                    "valid_types" => #allowed_second_args.to_owned(),
                                },
                            }, "invalid_second_par_type");
                        }
                    }
                }
                _ => {
                    error!("LS Error: arguments not the same type but was already checked.");
                }
            }
        }
    } else {
        let match_first_par = list_match_first_par(&fields, &ast);
        // Primary token is the main token that has to be set before adding data. (issue #36)
        let primary_token = get_primary_token(&fields);
        let primary_token_check = if let Some((primary_token, primary_token_ref)) = primary_token {
            let primary_token_ref = match primary_token_ref {
                Some(reference) => quote! { Some(#reference.to_owned()) },
                None => quote! { None },
            };
            quote! {
                let primary_token_filled = new_self.#primary_token.is_some();
                let primary_token_ref: Option<String> = #primary_token_ref;
            }
        } else {
            quote! {
                let primary_token_filled = true;
                let primary_token_ref: Option<String> = None;
            }
        };
        quote! {
            let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics).unwrap();
            #primary_token_check
            // match on first parameter
            let arg0 = token.arguments.get(0).unwrap();
            match &arg0.value {
                TokenValue::TVReference(value) => {
                    debug!("Matching {} in {}", value, stringify!(#name));
                    match value.as_ref() as &str {
                        #match_first_par
                        _ => {
                            // If nothing changed
                            if new_self == Self::default() {
                                return (LoopControl::ErrBreak, new_self);
                            }
                            // Go back up to parent
                            return (LoopControl::Break, new_self);
                        }
                    }
                }
                _ => {
                    // This should already be handled by lexer, but in case of
                    diagnostics.add_message(DMExtraInfo {
                        range: node.get_range(),
                        message_template_data: hash_map! {
                            "found_type" => argument_to_token_name(&arg0.value),
                        },
                    }, "first_argument_not_a_ref");
                }
            }
        }
    };
    // Create the list of allowed tokens
    // This is used for checking in `Vec<>`
    let allowed_tokens = if struct_info.second_par_check {
        let token_name = struct_info.token.unwrap();
        quote! { vec![TokenValue::TVReference(#token_name.to_owned())] }
    } else {
        let list = list_allowed_tokens(&fields, &ast);
        quote! { vec![#list] }
    };

    quote! {
        impl TokenDeserialize for #name {
            #[allow(unused_variables)]
            fn deserialize_general_token(
                mut cursor: &mut df_ls_syntax_analysis::TreeCursor,
                source: &str,
                mut diagnostics: &mut df_ls_diagnostics::DiagnosticsInfo,
                mut new_self: Self,
            ) -> (df_ls_syntax_analysis::LoopControl, Self) {
                use df_ls_syntax_analysis::{argument_to_token_name, LoopControl, TreeCursor,
                    TokenValue, Token};
                use df_ls_diagnostics::{DiagnosticsInfo, DMExtraInfo, hash_map};

                let node = cursor.node();
                #parameter_checks
                (LoopControl::DoNothing, new_self)
            }

            fn get_allowed_tokens() -> Option<Vec<df_ls_syntax_analysis::TokenValue>> {
                use df_ls_syntax_analysis::TokenValue;
                Some(#allowed_tokens)
            }
        }

        impl df_ls_syntax_analysis::TryFromArgument for #name {
            fn try_from_argument(
                _arg_opt: Option<&df_ls_syntax_analysis::Argument>,
                _source: &str,
                _diagnostics: &mut df_ls_diagnostics::DiagnosticsInfo,
                _add_diagnostics_on_err: bool,
            ) -> Result<Self, ()> {
                unimplemented!("This function should not be used for this type.");
            }

            fn expected_argument_count() -> (u32, u32) {
                (1, 1)
            }

            fn expected_argument_types() -> String {
                stringify!(#name).to_owned()
            }
        }
    }
}

fn list_match_first_par(
    struct_fields: &[syn::Field],
    ast: &syn::DeriveInput,
) -> proc_macro2::TokenStream {
    let mut parse_gen = quote! {};
    for (_i, field) in struct_fields.iter().enumerate() {
        let ident = field.ident.as_ref().unwrap();
        let token_info = get_token_de_info(&field);
        let ident_is_vec = common::check_type_is_vec(&field.ty);
        let token_ref = token_info.token;
        // Change what to do when duplicate is reached
        let on_duplicate = if token_info.on_duplicate_to_parent {
            quote! {
                // Go back up to parent
                return (LoopControl::Break, new_self);
            }
        } else {
            let token_ref = token_ref.clone().unwrap_or_default();
            let primary_token = match get_primary_token(&struct_fields) {
                Some((_, Some(token_name))) => token_name,
                _ => "<unknown>".to_owned(),
            };
            if token_info.on_duplicate_error {
                quote! {
                    diagnostics.add_message(DMExtraInfo {
                        range: node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => format!("`{}`", #token_ref),
                            "parent_token" => format!("`{}`", #primary_token),
                        },
                    }, "duplicate_token_error");
                    if let Err(_) = Token::consume_token(&mut cursor) {
                        return (LoopControl::Break, new_self);
                    }
                    return (LoopControl::DoNothing, new_self);
                }
            } else {
                quote! {
                    diagnostics.add_message(DMExtraInfo {
                        range: node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => format!("`{}`", #token_ref),
                            "parent_token" => format!("`{}`", #primary_token),
                        },
                    }, "duplicate_token_warn");
                    if let Err(_) = Token::consume_token(&mut cursor) {
                        return (LoopControl::Break, new_self);
                    }
                    return (LoopControl::DoNothing, new_self);
                }
            }
        };
        // If this token is not the primary token, add check if that is filled. (issue #36)
        let primary_token_check = if !token_info.primary_token {
            quote! {
                if !primary_token_filled {
                    if let Some(primary_token_ref) = primary_token_ref{
                        diagnostics.add_message(DMExtraInfo {
                            range: node.get_range(),
                            message_template_data: hash_map! {
                                "expected_tokens" => format!("`{}`", primary_token_ref),
                            },
                        }, "token_is_missing");
                    } else {
                        diagnostics.add_message(DMExtraInfo {
                            range: node.get_range(),
                            message_template_data: hash_map! {
                                "expected_tokens" => "<Unknown>".to_owned(),
                            },
                        }, "token_is_missing");
                    }
                    return (LoopControl::ErrBreak, new_self);
                }
            }
        } else {
            quote! {}
        };
        // Add if `token = "SOME"` is set
        if let Some(token_ref) = token_ref {
            match &ast.data {
                syn::Data::Struct(_x) => {
                    if ident_is_vec {
                        parse_gen = quote! {
                            #parse_gen
                            #token_ref => {
                                #primary_token_check
                                if let Ok(mut value) = TokenDeserialize::deserialize_tokens(
                                    &mut cursor,
                                    &source,
                                    &mut diagnostics,
                                ){
                                    // Append because we want to parse a whole list, not just 1 item
                                    new_self.#ident.append(&mut value);
                                }
                                // Don't go to next token, we are still matching this token.
                                return (LoopControl::Continue, new_self);
                            }
                        };
                        // Alias
                        if let Some(alias) = token_info.alias {
                            parse_gen = quote! {
                                #parse_gen
                                #alias => {
                                    #primary_token_check
                                    diagnostics.add_message(
                                        DMExtraInfo {
                                            range: arg0.node.get_range(),
                                            message_template_data: hash_map! {
                                                "alias_name" => format!("`{}`", #alias),
                                                "suggested_name" => format!("`{}`", #token_ref),
                                            },
                                        },
                                        "alias",
                                    );
                                    if let Ok(mut value) = TokenDeserialize::deserialize_tokens(
                                        &mut cursor,
                                        &source,
                                        &mut diagnostics,
                                    ){
                                        // Append because we want to parse a whole list, not just 1 item
                                        new_self.#ident.append(&mut value);
                                    }
                                    // Don't go to next token, we are still matching this token.
                                    return (LoopControl::Continue, new_self);
                                }
                            };
                        }
                    } else {
                        parse_gen = quote! {
                            #parse_gen
                            #token_ref => {
                                if new_self.#ident.is_some() {
                                    #on_duplicate
                                }
                                #primary_token_check
                                let value = TokenDeserialize::deserialize_tokens(
                                    &mut cursor,
                                    &source,
                                    &mut diagnostics,
                                );
                                match value {
                                    Ok(value) => {
                                        new_self.#ident = Some(value);
                                    }
                                    Err(_) => { /* Error is already in diagnostics */ }
                                }
                            }
                        };
                        // Alias
                        if let Some(alias) = token_info.alias {
                            parse_gen = quote! {
                                #parse_gen
                                #alias => {
                                    if new_self.#ident.is_some() {
                                        #on_duplicate
                                    }
                                    #primary_token_check
                                    diagnostics.add_message(
                                        DMExtraInfo {
                                            range: arg0.node.get_range(),
                                            message_template_data: hash_map! {
                                                "alias_name" => format!("`{}`", #alias),
                                                "suggested_name" => format!("`{}`", #token_ref),
                                            },
                                        },
                                        "alias",
                                    );
                                    let value = TokenDeserialize::deserialize_tokens(
                                        &mut cursor,
                                        &source,
                                        &mut diagnostics,
                                    );
                                    match value {
                                        Ok(value) => {
                                            new_self.#ident = Some(value);
                                        }
                                        Err(_) => { /* Error is already in diagnostics */ }
                                    }
                                }
                            };
                        }
                    }
                }
                syn::Data::Enum(_x) => {
                    parse_gen = quote! {
                        #parse_gen
                        #token_ref => {
                            #primary_token_check
                            let value = TokenDeserialize::deserialize_tokens(
                                &mut cursor,
                                &source,
                                &mut diagnostics,
                            );
                            match value {
                                Ok(value) => {
                                    new_self = Self::#ident(value);
                                    return (LoopControl::Break, new_self);
                                }
                                Err(_) => { /* Error is already in diagnostics */ }
                            }
                            return (LoopControl::Continue, new_self);
                        }
                    };
                    // Alias
                    if let Some(alias) = token_info.alias {
                        parse_gen = quote! {
                            #parse_gen
                            #alias => {
                                #primary_token_check
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: arg0.node.get_range(),
                                        message_template_data: hash_map! {
                                            "alias_name" => format!("`{}`", #alias),
                                            "suggested_name" => format!("`{}`", #token_ref),
                                        },
                                    },
                                    "alias",
                                );
                                let value = TokenDeserialize::deserialize_tokens(
                                    &mut cursor,
                                    &source,
                                    &mut diagnostics,
                                );
                                match value {
                                    Ok(value) => {
                                        new_self = Self::#ident(value);
                                        return (LoopControl::Break, new_self);
                                    }
                                    Err(_) => { /* Error is already in diagnostics */ }
                                }
                                return (LoopControl::Continue, new_self);
                            }
                        };
                    }
                }
                syn::Data::Union(_x) => {
                    unimplemented!("Union is not implemented for TokenDeserialize.");
                }
            }
        }
    }
    parse_gen
}

fn list_match_second_par(struct_fields: &[syn::Field]) -> proc_macro2::TokenStream {
    let mut parse_gen = quote! {};
    for (_i, field) in struct_fields.iter().enumerate() {
        let ident = field.ident.as_ref().unwrap();
        let token_info = get_token_de_info(&field);
        let ident_is_vec = common::check_type_is_vec(&field.ty);
        let token_ref = token_info.token;
        // Add if `token = "SOME"` is set
        if let Some(token_ref) = token_ref {
            if ident_is_vec {
                parse_gen = quote! {
                    #parse_gen
                    #token_ref => {
                        if let Ok(mut value) = TokenDeserialize::deserialize_tokens(
                            &mut cursor,
                            &source,
                            &mut diagnostics,
                        ){
                            // Append because we want to parse a whole list, not just 1 item
                            new_self.#ident.append(&mut value);
                        }
                        // Don't go to next token, we are still matching this token.
                        return (LoopControl::Continue, new_self);
                    }
                };
            } else {
                panic!(
                    "Only `Vec<>` types are supported for member \
                    field of a struct that uses `token_de(second_par_check)`."
                );
            }
        }
    }
    parse_gen
}

pub(super) fn list_allowed_tokens(
    struct_fields: &[syn::Field],
    ast: &syn::DeriveInput,
) -> proc_macro2::TokenStream {
    let mut parse_gen = quote! {};
    for (_i, field) in struct_fields.iter().enumerate() {
        let token_info = get_token_de_info(&field);
        let token_ref = token_info.token;

        // Add if `token = "SOME"` is set
        if let Some(token_ref) = token_ref {
            match &ast.data {
                syn::Data::Struct(_x) => {
                    parse_gen = quote! {
                        #parse_gen TokenValue::TVReference(#token_ref.to_owned()),
                    };
                }
                syn::Data::Enum(_x) => {
                    parse_gen = quote! {
                        #parse_gen TokenValue::TVReference(#token_ref.to_owned()),
                    };
                }
                syn::Data::Union(_x) => {
                    unimplemented!("Union is not implemented for TokenDeserialize.");
                }
            }
        }
    }
    parse_gen
}

pub(super) fn list_allowed_tokens_strings(
    struct_fields: &[syn::Field],
    ast: &syn::DeriveInput,
) -> Vec<String> {
    let mut list = vec![];
    for (_i, field) in struct_fields.iter().enumerate() {
        let token_info = get_token_de_info(&field);
        let token_ref = token_info.token;

        // Add if `token = "SOME"` is set
        if let Some(token_ref) = token_ref {
            match &ast.data {
                syn::Data::Struct(_x) => {
                    list.push(token_ref);
                }
                syn::Data::Enum(_x) => {
                    list.push(token_ref);
                }
                syn::Data::Union(_x) => {
                    unimplemented!("Union is not implemented for TokenDeserialize.");
                }
            }
        }
    }
    list
}

fn get_primary_token(struct_fields: &[syn::Field]) -> Option<(Ident, Option<String>)> {
    let mut result = None;
    for (_i, field) in struct_fields.iter().enumerate() {
        let ident = field.ident.as_ref().unwrap();
        let token_info = get_token_de_info(&field);
        if token_info.primary_token {
            let token_ref = token_info.token;
            if result.is_none() {
                result = Some((ident.clone(), token_ref));
            } else {
                panic!("ERROR: Only one primary_token can be set.");
            }
        }
    }
    result
}
