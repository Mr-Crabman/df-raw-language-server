use crate::ReferenceTo;
pub use df_ls_derive::Referenceable;

pub trait Referenceable {
    fn get_reference(&self) -> Option<ReferenceTo<Self>>
    where
        Self: Sized;

    fn get_ref_type() -> &'static str;
}
