use super::super::{Argument, Token, TokenValue, TryFromArgument};
use super::{LoopControl, TokenDeserialize};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Deserialize a token with following pattern: `[REF]`
impl TokenDeserialize for () {
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        token.check_token::<Self>(&source, &mut diagnostics, true)?;
        // Does not expect any arguments except for token name
        Ok(())
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

impl TryFromArgument for () {
    fn try_from_argument(
        _arg_opt: Option<&Argument>,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        _add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        unimplemented!("This function should not be used for this type.");
    }

    fn expected_argument_count() -> (u32, u32) {
        (0, 0)
    }

    fn expected_argument_types() -> String {
        "No arguments".to_owned()
    }
}

// Prevent having to copy post following code with little changes
// for each of the tuple amount
macro_rules! token_de_tuples {
    ( $($x:ident,)+ ) => {
        /// Deserialize a token with following pattern: `[REF:..:..:..]`
        impl<$($x),+> TokenDeserialize for ($($x,)+)
        where
            $(
                $x: Default + TryFromArgument,
            )+
        {
            fn deserialize_tokens(
                mut cursor: &mut TreeCursor,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
            ) -> Result<Self, ()> {
                // Check that is has no additional arguments
                let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
                Token::consume_token(&mut cursor)?;
                token.check_token::<Self>(&source, &mut diagnostics, true)?;
                // Create Tuple
                let mut arg_iter = token.arguments.into_iter();
                // Skip first argument (token_reference)
                arg_iter.next();
                let result = (
                    $(
                        {
                            // Go to second argument
                            let arg = arg_iter.next();
                            $x::try_from_argument(arg.as_ref(),
                                source,
                                &mut diagnostics,
                                true,
                            )?
                        },
                    )+
                );
                Ok(result)
            }

            fn deserialize_general_token(
                _cursor: &mut TreeCursor,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                new_self: Self,
            ) -> (LoopControl, Self) {
                (LoopControl::DoNothing, new_self)
            }

            fn get_vec_loopcontrol() -> LoopControl {
                LoopControl::DoNothing
            }

            fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
                None
            }
        }

        impl<$($x),+> TryFromArgument for ($($x,)+)
        where
            $(
                $x: Default + TryFromArgument,
            )+
        {
            fn try_from_argument(
                _arg_opt: Option<&Argument>,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                _add_diagnostics_on_err: bool,
            ) -> Result<Self, ()> {
                unimplemented!("This function should not be used.");
            }

            fn expected_argument_count() -> (u32, u32) {
                let mut argument_count_min = 0;
                let mut argument_count_max = 0;
                $(
                    {
                        let (min, max) = <$x>::expected_argument_count();
                        argument_count_min += min;
                        argument_count_max += max;
                    }
                )+
                (argument_count_min, argument_count_max)
            }

            fn expected_argument_types() -> String {
                "Expected multiple arguments".to_owned()
            }
        }
    };
}

token_de_tuples!(T1,);
token_de_tuples!(T1, T2,);
token_de_tuples!(T1, T2, T3,);
token_de_tuples!(T1, T2, T3, T4,);
token_de_tuples!(T1, T2, T3, T4, T5,);
token_de_tuples!(T1, T2, T3, T4, T5, T6,);
token_de_tuples!(T1, T2, T3, T4, T5, T6, T7,);
token_de_tuples!(T1, T2, T3, T4, T5, T6, T7, T8,);
token_de_tuples!(T1, T2, T3, T4, T5, T6, T7, T8, T9,);
token_de_tuples!(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,);
