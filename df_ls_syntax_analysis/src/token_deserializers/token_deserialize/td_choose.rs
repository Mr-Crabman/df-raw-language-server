use super::super::{argument_to_token_name, Argument, Token, TokenValue, TryFromArgument};
use super::{LoopControl, TokenDeserialize};
use df_ls_core::Choose;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Deserialize a token with following pattern: `[REF:Choose<T1,T2>]`
impl<T1, T2> TokenDeserialize for Choose<T1, T2>
where
    T1: Default + TryFromArgument,
    T2: Default + TryFromArgument,
{
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        token.check_token::<Self>(&source, &mut diagnostics, true)?;

        Self::try_from_argument(token.arguments.get(1), source, &mut diagnostics, true)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

// -------------------------Convert to TokenValue -----------------------

impl<T1, T2> TryFromArgument for Choose<T1, T2>
where
    T1: TryFromArgument,
    T2: TryFromArgument,
{
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            let option1 = T1::try_from_argument(arg_opt, source, &mut diagnostics, false);
            let option2 = T2::try_from_argument(arg_opt, source, &mut diagnostics, false);
            if let Ok(option1) = option1 {
                // Use option 1, even if option2 is valid
                // Add potential error messages
                T1::try_from_argument(arg_opt, source, &mut diagnostics, add_diagnostics_on_err)?;
                Ok(Choose::Choice1(option1))
            } else if let Ok(option2) = option2 {
                // Add potential error messages
                T2::try_from_argument(arg_opt, source, &mut diagnostics, add_diagnostics_on_err)?;
                Ok(Choose::Choice2(option2))
            } else {
                if add_diagnostics_on_err {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: arg.node.get_range(),
                            message_template_data: hash_map! {
                                "expected_parameters" => Self::expected_argument_types(),
                                "found_parameters" => argument_to_token_name(&arg.value),
                            },
                        },
                        "wrong_arg_type",
                    );
                }
                Err(())
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_count() -> (u32, u32) {
        let (min1, max1) = T1::expected_argument_count();
        let (min2, max2) = T2::expected_argument_count();
        let arg_min = std::cmp::min(min1, min2);
        let arg_max = std::cmp::max(max1, max2);
        (arg_min, arg_max)
    }

    fn expected_argument_types() -> String {
        format!(
            "({} or {})",
            T1::expected_argument_types(),
            T2::expected_argument_types()
        )
    }
}

// -------------------------Convert from TokenValue -----------------------

impl<T1, T2> From<Choose<T1, T2>> for TokenValue
where
    TokenValue: From<T1>,
    TokenValue: From<T2>,
{
    fn from(item: Choose<T1, T2>) -> TokenValue {
        match item {
            Choose::Choice1(v) => TokenValue::from(v),
            Choose::Choice2(v) => TokenValue::from(v),
        }
    }
}

impl<T1, T2> From<Option<Choose<T1, T2>>> for TokenValue
where
    TokenValue: From<T1>,
    TokenValue: From<T2>,
{
    fn from(item: Option<Choose<T1, T2>>) -> TokenValue {
        match item {
            Some(item) => match item {
                Choose::Choice1(v) => TokenValue::from(v),
                Choose::Choice2(v) => TokenValue::from(v),
            },
            None => TokenValue::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_choose_correct() {
        let source = "header
            [REF:string]
            [REF:83]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test String -> Choose<u8, String> ---
        // Deserialize the AST
        let test_string: Choose<u8, String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(Choose::Choice2("string".to_owned()), test_string);

        // ---- Test u8 -> Choose<u8, String> ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u8: Choose<u8, String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(Choose::Choice1(83u8), test_u8);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_choose_enum_correct() {
        let source = "header
            [REF:string]
            [REF:83]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test String -> Choose<u8, String> ---
        // Deserialize the AST
        let test_string: (Choose<u8, String>,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((Choose::Choice2("string".to_owned()),), test_string);

        // ---- Test u8 -> Choose<u8, String> ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u8: (Choose<u8, String>,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((Choose::Choice1(83u8),), test_u8);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
