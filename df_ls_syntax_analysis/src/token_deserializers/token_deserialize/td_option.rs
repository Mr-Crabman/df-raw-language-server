use super::super::{Argument, Token, TokenValue, TryFromArgument};
use super::{LoopControl, TokenDeserialize};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Deserialize a token with following pattern: `[REF:Option<T>]`
impl<T> TokenDeserialize for Option<T>
where
    T: Default + TryFromArgument,
    Option<T>: TryFromArgument + Default,
{
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        token.check_token::<Self>(&source, &mut diagnostics, true)?;

        Self::try_from_argument(token.arguments.get(1), source, &mut diagnostics, true)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

impl<T> TryFromArgument for Option<T>
where
    T: TryFromArgument,
{
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if arg_opt.is_some() {
            match T::try_from_argument(arg_opt, source, &mut diagnostics, add_diagnostics_on_err) {
                Ok(value) => Ok(Some(value)),
                Err(_) => Err(()),
            }
        } else {
            Ok(None)
        }
    }

    fn expected_argument_count() -> (u32, u32) {
        (0, T::expected_argument_count().1)
    }

    fn expected_argument_types() -> String {
        format!(
            "(Remove/Optional argument or {})",
            T::expected_argument_types()
        )
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_option_string_correct() {
        let source = "header
            [REF:optional argument]
            [REF]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        let test1: Option<String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(Some("optional argument".to_owned()), test1);

        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test2: Option<String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(None, test2);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_option_string_to_many_args() {
        let source = "header
            [REF::]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        match Option::<String>::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
        {
            Ok(_) => panic!("This should be an error, got correct result"),
            Err(_) => {}
        }

        let diagnostic_list = diagnostic_info.diagnostics;
        test_common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_number".to_owned()]);
    }

    #[test]
    fn test_option_string_with_empty_arg() {
        let source = "header
            [REF:]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        match Option::<String>::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
        {
            Ok(_) => panic!("This should be an error, got correct result"),
            Err(_) => {}
        }

        let diagnostic_list = diagnostic_info.diagnostics;
        test_common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    }

    #[test]
    fn test_option_string_wrong_arg_type() {
        let source = "header
            [REF:56]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        match Option::<String>::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
        {
            Ok(_) => panic!("This should be an error, got correct result"),
            Err(_) => {}
        }

        let diagnostic_list = diagnostic_info.diagnostics;
        test_common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    }
}
