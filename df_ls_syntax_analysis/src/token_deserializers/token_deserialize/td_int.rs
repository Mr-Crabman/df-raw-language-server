use super::super::{argument_to_token_name, Argument, Token, TokenValue, TryFromArgument};
use super::{LoopControl, TokenDeserialize};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Prevent having to copy post following code with little changes
// for each of the integer types
macro_rules! token_de_integer {
    ( $x:ty ) => {
        /// Deserialize a token with following pattern: `[REF:INT]`
        impl TokenDeserialize for $x {
            fn deserialize_tokens(
                mut cursor: &mut TreeCursor,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
            ) -> Result<Self, ()> {
                // Get arguments from token
                let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
                Token::consume_token(&mut cursor)?;
                token.check_token::<Self>(&source, &mut diagnostics, true)?;

                Self::try_from_argument(token.arguments.get(1), source, &mut diagnostics, true)
            }

            fn deserialize_general_token(
                _cursor: &mut TreeCursor,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                new_self: Self,
            ) -> (LoopControl, Self) {
                (LoopControl::DoNothing, new_self)
            }

            fn get_vec_loopcontrol() -> LoopControl {
                LoopControl::DoNothing
            }

            fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
                None
            }
        }
    };
}

// Big numbers like i128 should not be used.
// token_de_integer!(i128);
token_de_integer!(i64);
token_de_integer!(i32);
token_de_integer!(i16);
token_de_integer!(i8);

// Big numbers like u128 should not be used.
// token_de_integer!(u128);
// Can not cast i64 to u64 without loss
// token_de_integer!(u64);
token_de_integer!(u32);
token_de_integer!(u16);
token_de_integer!(u8);

// -------------------------Convert to TokenValue -----------------------

// Prevent having to copy post following code with little changes
// for each of the integer types
macro_rules! token_value_into_integer {
    ( $x:ty, $t:literal ) => {
        impl TryFromArgument for $x {
            fn try_from_argument(
                arg_opt: Option<&Argument>,
                _source: &str,
                diagnostics: &mut DiagnosticsInfo,
                add_diagnostics_on_err: bool,
            ) -> Result<Self, ()> {
                if let Some(arg) = arg_opt {
                    match arg.value {
                        TokenValue::TVInteger(i64_value) => {
                            if i64_value > (<$x>::MAX as i64) {
                                if add_diagnostics_on_err {
                                    diagnostics.add_message(
                                        DMExtraInfo {
                                            range: arg.node.get_range(),
                                            message_template_data: hash_map! {
                                                "max_value" => <$x>::MAX.to_string(),
                                            },
                                        },
                                        "too_large_int",
                                    );
                                }
                                return Err(());
                            }
                            if i64_value < (<$x>::MIN as i64) {
                                if add_diagnostics_on_err {
                                    diagnostics.add_message(
                                        DMExtraInfo {
                                            range: arg.node.get_range(),
                                            message_template_data: hash_map! {
                                                "min_value" => <$x>::MIN.to_string(),
                                            },
                                        },
                                        "too_small_int",
                                    );
                                }
                                return Err(());
                            }
                            Ok(i64_value as $x)
                        }
                        _ => {
                            if add_diagnostics_on_err {
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: arg.node.get_range(),
                                        message_template_data: hash_map! {
                                            "expected_parameters" => Self::expected_argument_types(),
                                            "found_parameters" => argument_to_token_name(&arg.value),
                                        },
                                    },
                                    "wrong_arg_type",
                                );
                            }
                            Err(())
                        }
                    }
                } else {
                    Err(())
                }
            }

            fn expected_argument_count() -> (u32, u32) {
                (1, 1)
            }

            fn expected_argument_types() -> String {
                format!("Integer ({})", $t)
            }
        }
    };
}

// token_value_into_integer!(i128);
token_value_into_integer!(i64, "i64");
token_value_into_integer!(i32, "i32");
token_value_into_integer!(i16, "i16");
token_value_into_integer!(i8, "i8");

// token_value_into_integer!(u128);
// Can not cast i64 to u64 without loss
// token_value_into_integer!(u64);
token_value_into_integer!(u32, "u32");
token_value_into_integer!(u16, "u16");
token_value_into_integer!(u8, "u8");

// -------------------------Convert from TokenValue -----------------------

macro_rules! token_value_from_integer {
    ( $x:ty ) => {
        impl From<$x> for TokenValue {
            fn from(item: $x) -> TokenValue {
                TokenValue::TVInteger(item as i64)
            }
        }

        impl From<Option<$x>> for TokenValue {
            fn from(item: Option<$x>) -> TokenValue {
                match item {
                    Some(v) => TokenValue::TVInteger(v as i64),
                    None => TokenValue::TVEmpty,
                }
            }
        }
    };
}

// token_value_from_integer!(i128);
token_value_from_integer!(i64);
token_value_from_integer!(i32);
token_value_from_integer!(i16);
token_value_from_integer!(i8);

// token_value_from_integer!(u128);
// Can not cast i64 to u64 without loss
// token_value_from_integer!(u64);
token_value_from_integer!(u32);
token_value_from_integer!(u16);
token_value_from_integer!(u8);

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_integer_correct() {
        let source = "header
            [REF:127]
            [REF:-127]
            [REF:-20]
            [REF:1555]
            [REF:4294967295]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test i8 ---
        // Deserialize the AST
        let test_i8: i8 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(127i8, test_i8);

        // ---- Test i8 as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i8_t: (i8,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((-127i8,), test_i8_t);

        // ---- Test i16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i16: i16 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(-20i16, test_i16);

        // ---- Test u16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u16: u16 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(1555u16, test_u16);

        // ---- Test u32 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u32: u32 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(4294967295u32, test_u32);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_integer_overflow() {
        let source = "header
            [REF:130]
            [REF:-130]
            [REF:-32769]
            [REF:-1]
            [REF:4294967296]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test i8 ---
        // Deserialize the AST
        let test_i8: Option<i8> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_i8);

        // ---- Test i8 as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i8_t: Option<(i8,)> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_i8_t);

        // ---- Test i16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i16: Option<i16> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_i16);

        // ---- Test u16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u16: Option<u16> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_u16);

        // ---- Test u32 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u32: Option<u32> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_u32);

        test_common::assert_diagnostic_codes(
            &diagnostic_info.diagnostics,
            vec![
                "too_large_int".to_owned(),
                "too_small_int".to_owned(),
                "too_small_int".to_owned(),
                "too_small_int".to_owned(),
                "too_large_int".to_owned(),
            ],
        );
    }
}
