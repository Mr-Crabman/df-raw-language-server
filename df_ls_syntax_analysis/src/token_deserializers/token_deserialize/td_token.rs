use super::super::{Argument, Token, TokenValue};
use super::{LoopControl, TokenDeserialize};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Deserialize a token with any pattern
impl TokenDeserialize for Token {
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Set the whole `general_token` as token node
        let node = cursor.node();
        let mut new_self = Self {
            node: Some(node),
            arguments: vec![],
        };
        // Go into `general_token`
        cursor.goto_first_child();
        loop {
            let node = cursor.node();
            match node.kind().as_ref() {
                "[" | "]" => { /* Do nothing */ }
                "token_value_reference" => {
                    if let Ok(value) =
                        TokenDeserialize::deserialize_tokens(&mut cursor, &source, &mut diagnostics)
                    {
                        let argument = Argument { node, value };
                        new_self.arguments.push(argument);
                    };
                }
                "general_token_values" => {
                    if let Ok(mut other_arguments) =
                        deserialize_general_token_values(&mut cursor, &source, &mut diagnostics)
                    {
                        new_self.arguments.append(&mut other_arguments);
                    }
                }
                "EOF" => break,
                others => {
                    error!("Found an unknown node of kind: {}", others);
                    break;
                }
            }
            // Check if there is a next sibling
            if !cursor.goto_next_sibling() {
                break;
            }
        }
        // Go out of `general_token`
        cursor.goto_parent();

        if new_self.arguments.is_empty() {
            // Something went wrong during parsing or this token is empty
            error!(
                "Token could not be parsed to get arguments: {:?}",
                cursor.node()
            );
            // Skip this token as it will never be parsed
            cursor.goto_next_sibling();
        }

        Ok(new_self)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

fn deserialize_general_token_values(
    mut cursor: &mut TreeCursor,
    source: &str,
    mut diagnostics: &mut DiagnosticsInfo,
) -> Result<Vec<Argument>, ()> {
    // Go into general_token
    cursor.goto_first_child();
    let mut argument_list = vec![];
    loop {
        let node = cursor.node();
        match node.kind().as_ref() {
            ":" => {}
            "token_value_integer"
            | "token_value_character"
            | "token_value_string"
            | "token_value_reference"
            | "token_value_empty" => {
                argument_list.push(Argument {
                    node,
                    value: TokenDeserialize::deserialize_tokens(
                        &mut cursor,
                        &source,
                        &mut diagnostics,
                    )?,
                });
            }
            "EOF" => break,
            others => {
                error!("Found an unknown node of kind: {}", others);
                break;
            }
        }
        // Check if there is a next sibling
        if !cursor.goto_next_sibling() {
            break;
        }
    }
    // Go out of general_token
    cursor.goto_parent();

    Ok(argument_list)
}
