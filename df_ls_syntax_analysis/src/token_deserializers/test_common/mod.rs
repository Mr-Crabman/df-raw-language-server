#![cfg(test)]

use df_ls_diagnostics::lsp_types::*;
use log::{Level, LevelFilter, Metadata, Record};
use pretty_assertions::assert_eq;

pub static LOGGER: Logger = Logger;
pub struct Logger;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Debug
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{:<5}:{} - {}",
                match record.level() {
                    Level::Error => "ERROR",
                    Level::Warn => "WARN",
                    Level::Info => "INFO",
                    Level::Debug => "DEBUG",
                    Level::Trace => "TRACE",
                },
                record.target(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}

pub fn setup_logger() {
    log::set_logger(&LOGGER).unwrap_or_else(|_| {});
    log::set_max_level(LevelFilter::Debug);
}

#[allow(dead_code)]
pub fn assert_diagnostic_codes(given_list: &[Diagnostic], expected_codes: Vec<String>) {
    assert_eq!(
        given_list.len(),
        expected_codes.len(),
        "Diagnostics codes lists are not the same size."
    );
    for (given, expected) in given_list.iter().zip(expected_codes.iter()) {
        if let Some(code) = &given.code {
            match code {
                NumberOrString::String(code) => {
                    assert_eq!(code, expected);
                }
                _ => panic!("Diagnostic code is not of type `String`."),
            }
        } else {
            panic!("One diagnostic message does not have a code");
        }
    }
}

#[allow(dead_code)]
pub fn assert_diagnostic_range(given_list: &[Diagnostic], expected_ranges: Vec<Range>) {
    assert_eq!(
        given_list.len(),
        expected_ranges.len(),
        "Diagnostics ranges lists are not the same size."
    );
    for (given, range) in given_list.iter().zip(expected_ranges.iter()) {
        assert_eq!(&given.range, range);
    }
}
