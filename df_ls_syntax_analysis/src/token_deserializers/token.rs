use super::{argument_to_token_name, TokenValue, TryFromArgument};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::{Node, TreeCursor};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Token {
    pub node: Option<Node>,
    pub arguments: Vec<Argument>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Argument {
    pub node: Node,
    pub value: TokenValue,
}

impl Token {
    pub fn get_argument(&self, index: usize) -> Result<&Argument, ()> {
        match self.arguments.get(index) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }

    pub fn check_token<T: TryFromArgument>(
        &self,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        self.check_token_arg0(source, &mut diagnostics, add_diagnostics_on_err)?;
        // Check length
        self.check_arguments_length::<T>(source, &mut diagnostics, add_diagnostics_on_err)
    }

    pub fn check_token_arg0(
        &self,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        // First Token is always a REF
        let first_arg = self.arguments.get(0).unwrap();
        if let TokenValue::TVReference(_) = first_arg.value {
            // Correct
        } else {
            // Incorrect
            if add_diagnostics_on_err {
                diagnostics.add_message(
                    DMExtraInfo {
                        range: first_arg.node.get_range(),
                        message_template_data: hash_map! {
                            "expected_parameters" => "Reference".to_owned(),
                            "found_parameters" => argument_to_token_name(&first_arg.value),
                        },
                    },
                    "wrong_arg_type",
                );
            }
            return Err(());
        }
        Ok(())
    }

    pub fn check_arguments_length<T: TryFromArgument>(
        &self,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        let expected_len = T::expected_argument_count();

        self.check_arguments_length_fixed(
            expected_len,
            source,
            &mut diagnostics,
            add_diagnostics_on_err,
        )
    }

    /// Check the amount of arguments (token name excluded)
    /// Format: `(min, max)`
    /// Example: `[REF:string:5:'a']` would be `(3,3)`
    pub fn check_arguments_length_fixed(
        &self,
        expected_len: (u32, u32),
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        let (expected_len_min, expected_len_max) = expected_len;
        let current_len = self.arguments.len() as u32 - 1; // `-1` because first reference does not count
        if current_len >= expected_len_min && current_len <= expected_len_max {
            Ok(())
        } else {
            if add_diagnostics_on_err {
                if expected_len_min == expected_len_max {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: self.node.as_ref().unwrap().get_range(),
                            message_template_data: hash_map! {
                                "expected_parameters_num" => expected_len_min.to_string(),
                                "found_parameters_num" => current_len.to_string(),
                            },
                        },
                        "wrong_arg_number",
                    );
                } else {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: self.node.as_ref().unwrap().get_range(),
                            message_template_data: hash_map! {
                                "expected_parameters_num" => format!("between {} and {}", expected_len_min, expected_len_max),
                                "found_parameters_num" => current_len.to_string(),
                            },
                        },
                        "wrong_arg_number",
                    );
                }
            }
            Err(())
        }
    }

    pub fn consume_token(cursor: &mut TreeCursor) -> Result<(), ()> {
        match cursor.goto_next_sibling() {
            true => Ok(()),
            false => Err(()),
        }
    }
}
