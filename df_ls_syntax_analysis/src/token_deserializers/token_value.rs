#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};
use std::fmt::Display;

#[allow(clippy::upper_case_acronyms)]
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum TokenValue {
    TVInteger(i64),
    TVCharacter(char),
    TVString(String),
    TVReference(String),
    TVEmpty,
}

impl TokenValue {
    pub fn argument_to_token_name(argument: &TokenValue) -> String {
        match argument {
            TokenValue::TVInteger(_) => "Integer".to_string(),
            TokenValue::TVCharacter(_) => "char".to_string(),
            TokenValue::TVString(_) => "String".to_string(),
            TokenValue::TVReference(_) => "Reference".to_string(),
            TokenValue::TVEmpty => "Empty".to_string(),
        }
    }
}

impl Default for TokenValue {
    fn default() -> Self {
        TokenValue::TVEmpty
    }
}

impl Display for TokenValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "`{}`",
            match self {
                TokenValue::TVInteger(v) => v.to_string(),
                TokenValue::TVCharacter(v) => v.to_string(),
                TokenValue::TVString(v) => v.to_owned(),
                TokenValue::TVReference(v) => v.to_owned(),
                TokenValue::TVEmpty => "".to_owned(),
            }
        )
    }
}
