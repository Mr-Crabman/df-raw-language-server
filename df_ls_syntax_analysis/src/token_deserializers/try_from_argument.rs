use super::Argument;
use df_ls_diagnostics::DiagnosticsInfo;

#[allow(clippy::upper_case_acronyms)]
pub trait TryFromArgument: Sized {
    /// Performs the conversion.
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()>;

    /// Return as range: (min, max)
    fn expected_argument_count() -> (u32, u32);

    fn expected_argument_types() -> String;
}
