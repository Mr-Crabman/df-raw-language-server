mod common;

use common::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

/// Testing when `primary_token` has an error how it handles next tokens
/// (Regression test for issue #36)
/// (Slightly update because of #32)
#[test]
fn primary_token_error_1() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:9996]
        [ITEM:T1]
        [ITEM:T2]

    [TYPE1:REF2]
        [ITEM:T2]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "wrong_arg_type".to_owned(),
            "token_is_missing".to_owned(),
            "token_not_expected".to_owned(),
            "unchecked_code".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 3,
                    character: 11,
                },
                end: Position {
                    line: 3,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 17,
                },
                end: Position {
                    line: 9,
                    character: 4,
                },
            },
        ],
    );
}

/// Testing when `primary_token` has an error how it handles next tokens
/// (Regression test for issue #36)
#[test]
fn primary_token_error_2() {
    let source = "
    [MAIN:TYPE1]

    TYPE1:REF]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 13,
                },
                end: Position {
                    line: 3,
                    character: 14,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![],
            type_2: vec![],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "token_is_missing".to_owned(),
            "token_not_expected".to_owned(),
            "unchecked_code".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 17,
                },
                end: Position {
                    line: 11,
                    character: 4,
                },
            },
        ],
    );
}

/// Testing when `primary_token` has an error how it handles next tokens
/// (Regression test for issue #36)
#[test]
fn primary_token_error_3() {
    let source = "
    [MAIN:TYPE1]

        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![],
            type_2: vec![],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "token_is_missing".to_owned(),
            "token_not_expected".to_owned(),
            "unchecked_code".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 3,
                    character: 8,
                },
                end: Position {
                    line: 3,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 8,
                },
                end: Position {
                    line: 3,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 17,
                },
                end: Position {
                    line: 10,
                    character: 4,
                },
            },
        ],
    );
}
