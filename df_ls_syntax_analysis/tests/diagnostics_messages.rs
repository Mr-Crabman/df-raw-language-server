mod common;

use common::*;
use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
#[ignore = "This error can not happen because of lexer"]
fn message_first_argument_not_a_ref() {
    let source = "
    [MAIN:TYPE1]
    [TYPE1:DOG]
        [test]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["first_argument_not_a_ref".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 3,
                character: 9,
            },
            end: Position {
                line: 3,
                character: 13,
            },
        }],
    );
}

#[test]
fn message_token_not_expected() {
    let source = "
    [TEST]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 1,
                    character: 4,
                },
                end: Position {
                    line: 1,
                    character: 10,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("token_not_expected".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected a different token.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_token_is_missing() {
    let source = "
    [MAIN:TYPE1]
    [ITEM:DOG]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![
            Diagnostic {
                range: Range {
                    start: Position {
                        line: 2,
                        character: 4,
                    },
                    end: Position {
                        line: 2,
                        character: 14,
                    },
                },
                severity: Some(DiagnosticSeverity::Error),
                code: Some(NumberOrString::String("token_is_missing".to_owned())),
                source: Some("DF RAW Language Server".to_owned()),
                message: "Suggest adding one of following tokens here: `TYPE1`.".to_owned(),
                ..Default::default()
            },
            Diagnostic {
                range: Range {
                    start: Position {
                        line: 2,
                        character: 4,
                    },
                    end: Position {
                        line: 2,
                        character: 14,
                    },
                },
                severity: Some(DiagnosticSeverity::Error),
                code: Some(NumberOrString::String("token_not_expected".to_owned())),
                source: Some("DF RAW Language Server".to_owned()),
                message: "Expected a different token.".to_owned(),
                ..Default::default()
            }
        ]
    );
}

#[test]
fn message_invalid_second_par_type() {
    let source = "
    [MAIN:T]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 1,
                    character: 10,
                },
                end: Position {
                    line: 1,
                    character: 11,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("invalid_second_par_type".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "`T` is not a valid type in `MAIN`, valid types are \
                `TYPE1`, `TYPE2`, `TYPE3`, `TYPE4`, `PROFESSION`."
                .to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_unknown_token() {
    let source = "
    [MAIN:TYPE1]
    [TYPE1:DOG]
    [TEST]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 10,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("unknown_token".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Unrecognized token `TEST`.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_wrong_arg_number() {
    let source = "
    [MAIN:TYPE1]
    [TYPE1:DOG]
    [ITEM::::]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 14,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("wrong_arg_number".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected 1 arguments, found 4.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_wrong_arg_type() {
    let source = "
    [MAIN:TYPE1]
    [TYPE1:DOG]
    [ITEM:5]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 10,
                },
                end: Position {
                    line: 3,
                    character: 11,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("wrong_arg_type".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected Reference, found Integer.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_wrong_enum_value() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [GENDER:NOT]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![
            Diagnostic {
                range: Range {
                    start: Position {
                        line: 3,
                        character: 16,
                    },
                    end: Position {
                        line: 3,
                        character: 19,
                    },
                },
                severity: Some(DiagnosticSeverity::Error),
                code: Some(NumberOrString::String("wrong_enum_value".to_owned())),
                source: Some("DF RAW Language Server".to_owned()),
                message: "The value `NOT` is not allowed in this enum. Allowed value are: `MALE`, `FEMALE`, `OTHER`.".to_owned(),
                ..Default::default()
            }
        ]
    );
}

#[test]
fn message_too_large_int() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [DIFFICULTY:60000]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 20,
                },
                end: Position {
                    line: 3,
                    character: 25,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("too_large_int".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "This parameter does not accept values larger than 255.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_too_small_int() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [DIFFICULTY:-200]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 20,
                },
                end: Position {
                    line: 3,
                    character: 24,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("too_small_int".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "This parameter does not accept values smaller than 0.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
#[ignore = "This error can not happen because of lexer"]
fn message_char_wrong_quote() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [SYMBOL:'a!]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 16,
                },
                end: Position {
                    line: 3,
                    character: 19,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("char_wrong_quote".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected `'`(single quote) but found something `!`.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
#[ignore = "This error can not happen because of lexer"]
fn message_char_expected_more_chars() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [SYMBOL:']";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 16,
                },
                end: Position {
                    line: 3,
                    character: 17,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String(
                "char_expected_more_chars".to_owned()
            )),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected `'a'`(1 character in single quote) but found only 1 character."
                .to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
#[ignore = "This error can not happen because of lexer"]
fn message_expected_integer() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [DIFFICULTY:12.5]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 20,
                },
                end: Position {
                    line: 3,
                    character: 24,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("expected_integer".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected an Integer, but found `12.5`. \
                Could not covert this to an Integer: Not an Integer."
                .to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_duplicate_token_warn() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [DIFFICULTY:12]
        [DIFFICULTY:35]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                difficulty: Some(12),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 23,
                },
            },
            severity: Some(DiagnosticSeverity::Warning),
            code: Some(NumberOrString::String("duplicate_token_warn".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Duplicate token: `DIFFICULTY` already exists in this `PROFESSION`."
                .to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_duplicate_token_error() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [DIFFICULTY_HARD:12]
        [DIFFICULTY_HARD:35]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                difficulty_hard: Some(12),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 28,
                },
            },
            severity: Some(DiagnosticSeverity::Error),
            code: Some(NumberOrString::String("duplicate_token_error".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Duplicate token: `DIFFICULTY_HARD` already exists in this `PROFESSION`."
                .to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_reference_is_string() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:Dog]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("Dog".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 2,
                    character: 16,
                },
                end: Position {
                    line: 2,
                    character: 19,
                },
            },
            severity: Some(DiagnosticSeverity::Warning),
            code: Some(NumberOrString::String("reference_is_string".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected `Reference` but found `String`. Make sure to only use UPPERCASE, numbers and `_`."
                .to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_alias() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [JOB:DRINKING]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                name: vec![Reference("DRINKING".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 3,
                    character: 9,
                },
                end: Position {
                    line: 3,
                    character: 12,
                },
            },
            severity: Some(DiagnosticSeverity::Warning),
            code: Some(NumberOrString::String("alias".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "The token `JOB` is an alias, please use `NAME`.".to_owned(),
            ..Default::default()
        }]
    );
}

#[test]
fn message_unchecked_code() {
    let source = "
    [MAIN:PROFESSION]
    [PROFESSION:DOG]
        [TTTTTTT]
    [TEST]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    assert_eq!(
        diagnostic_list,
        vec![
            Diagnostic {
                range: Range {
                    start: Position {
                        line: 3,
                        character: 8,
                    },
                    end: Position {
                        line: 3,
                        character: 17,
                    },
                },
                severity: Some(DiagnosticSeverity::Error),
                code: Some(NumberOrString::String("unknown_token".to_owned())),
                source: Some("DF RAW Language Server".to_owned()),
                message: "Unrecognized token `TTTTTTT`.".to_owned(),
                ..Default::default()
            },
            Diagnostic {
                range: Range {
                    start: Position {
                        line: 3,
                        character: 17,
                    },
                    end: Position {
                        line: 4,
                        character: 10,
                    },
                },
                severity: Some(DiagnosticSeverity::Warning),
                code: Some(NumberOrString::String("unchecked_code".to_owned())),
                source: Some("DF RAW Language Server".to_owned()),
                message: "This code could not be checked because of previous error.".to_owned(),
                ..Default::default()
            }
        ]
    );
}
