mod common;

use common::*;
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_no_comments_1() {
    let source = "
    [MAIN:TYPE2][BIPEDAL:MONSTER][NAME:test][MAIN_PROFESSION:SLEEPING]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                main_profession: Some(Choose::Choice1(Reference("SLEEPING".to_owned()))),
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

#[test]
fn test_no_comments_with_error_1() {
    let source = "
    [MAIN:TYPE2][BIPEDAL:MONSTER][NAME:test][MAIN_PROFESSION:666]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                main_profession: None,
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 1,
                character: 61,
            },
            end: Position {
                line: 1,
                character: 64,
            },
        }],
    );
}

#[test]
fn test_no_comments_vec_with_error() {
    let source = "
    [MAIN:TYPE2][BIPEDAL:MONSTER][NAME:test][HOBBY:REF][HOBBY:String][HOBBY:666]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                hobbies: vec![
                    Choose::Choice1(Reference("REF".to_owned())),
                    Choose::Choice1(Reference("String".to_owned())),
                ],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "reference_is_string".to_owned(),
            "wrong_arg_type".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 1,
                    character: 62,
                },
                end: Position {
                    line: 1,
                    character: 68,
                },
            },
            Range {
                start: Position {
                    line: 1,
                    character: 76,
                },
                end: Position {
                    line: 1,
                    character: 79,
                },
            },
        ],
    );
}
