mod common;

use common::*;
use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_normal_case() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [NAME:TEST]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: vec![Reference("TEST".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

#[test]
fn test_alias() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [JOB:SLEEPER]
        [NAME:TEST]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: vec![
                    Reference("SLEEPER".to_owned()),
                    Reference("TEST".to_owned()),
                ],
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["alias".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 4,
                character: 9,
            },
            end: Position {
                line: 4,
                character: 12,
            },
        }],
    );
}
