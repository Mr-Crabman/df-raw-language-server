mod common;

use common::*;
use df_ls_core::{Choose, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_enum_value_derive() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [GENDER:FEMALE]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                gender: Some(GenderEnum::Female),
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

/// Regression test for #61
#[test]
fn test_enum_value_in_vec_derive() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [GENDER:FEMALE]
        [GENDER:MALE]
        [GENDER:OTHER]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                genders: vec![GenderEnum::Female, GenderEnum::Male, GenderEnum::Other],
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

/// Check if Enum allowed in `Choose`
#[test]
fn test_enum_value_in_choose_derive() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [HAND:LEFT]
    [PROFESSION:RAT]
        [HAND:AMBIDEXTROUS]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    handedness: Some(Choose::Choice2(HandednessEnum::Left)),
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("RAT".to_owned())),
                    handedness: Some(Choose::Choice2(HandednessEnum::Ambidextrous)),
                    ..Default::default()
                }
            ],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}
