mod common;
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_entity() {
    let source = "entity_default

    [OBJECT:ENTITY]
    
    [ENTITY:TEST]
        [SITE_CONTROLLABLE]
        [ALL_MAIN_POPS_CONTROLLABLE]
        [CREATURE:DWARF]
        [CREATURE:CRAB_MAN]
        [TRANSLATION:DWARF]
        [DIGGER:ITEM_WEAPON_PICK]
        [WEAPON:ITEM_WEAPON_AXE_BATTLE:COMMON]
        [WEAPON:ITEM_WEAPON_CROSSBOW]
            [AMMO:ITEM_AMMO_BOLTS]
        [ARMOR:ITEM_ARMOR_BREASTPLATE:COMMON]
        [ARMOR:ITEM_ARMOR_MAIL_SHIRT:COMMON]
        [HELM:ITEM_HELM_HELM:COMMON]
        [HELM:ITEM_HELM_CAP]
        [GLOVES:ITEM_GLOVES_GAUNTLETS:COMMON]
        [GLOVES:ITEM_GLOVES_GLOVES]
        [SHOES:ITEM_SHOES_SHOES:COMMON]
        [SHOES:ITEM_SHOES_BOOTS]
        [PANTS:ITEM_PANTS_PANTS:COMMON]
        [PANTS:ITEM_PANTS_GREAVES]
        [SHIELD:ITEM_SHIELD_SHIELD]
        [SHIELD:ITEM_SHIELD_BUCKLER]
        [SIEGEAMMO:ITEM_SIEGEAMMO_BALLISTA]
        [SIEGEAMMO:ITEM_SIEGEAMMO_SHARK_LAZORS]
        [TRAPCOMP:ITEM_TRAPCOMP_GIANTAXEBLADE]
        [TRAPCOMP:ITEM_TRAPCOMP_ENORMOUSCORKSCREW]
        [TOY:ITEM_TOY_PUZZLEBOX]
        [TOY:ITEM_TOY_BOAT]
        [CLOTHING]
        [SUBTERRANEAN_CLOTHING]
        [CURRENCY_BY_YEAR]
        [CURRENCY:COPPER:1]
        [CURRENCY:GOLD:10000]
        [SELECT_SYMBOL:WAR:NAME_WAR]
        [SUBSELECT_SYMBOL:WAR:VIOLENT]
        [SELECT_SYMBOL:BATTLE:NAME_BATTLE]
        [SUBSELECT_SYMBOL:BATTLE:VIOLENT]
        [CULL_SYMBOL:ALL:EVIL]
        [CULL_SYMBOL:ALL:FLOWERY]
        [EQUIPMENT_IMPROVEMENTS]
        [ITEM_IMPROVEMENT_MODIFIER:RINGS_HANGING:64]
        [ITEM_IMPROVEMENT_MODIFIER:SPIKES:384]
        [FRIENDLY_COLOR:1:0:1]
        [DEFAULT_SITE_TYPE:CAVE_DETAILED]
        [LIKES_SITE:CAVE_DETAILED]
        [TOLERATES_SITE:CITY]
        [TOLERATES_SITE:CAVE_DETAILED]
        [EXCLUSIVE_START_BIOME:MOUNTAIN]
        [SETTLEMENT_BIOME:ANY_FOREST]
        [SETTLEMENT_BIOME:MOUNTAIN]
        [BIOME_SUPPORT:MOUNTAIN:3]
        [BIOME_SUPPORT:ANY_RIVER:1]
        [ACTIVE_SEASON:AUTUMN]
        [SIEGER]
        [MAX_STARTING_CIV_NUMBER:100]
        [MAX_POP_NUMBER:10000]
        [MAX_SITE_POP_NUMBER:120]
        [RELIGION:PANTHEON]
        [RELIGION_SPHERE:FORTRESSES]
        [RELIGION_SPHERE:OCEANS]
        can also use SCHOLAR:ALL
        [SCHOLAR:PHILOSOPHER]
        [SCHOLAR:MATHEMATICIAN]
        [PERMITTED_JOB:MINER]
        [PERMITTED_JOB:CARPENTER]
        [PERMITTED_BUILDING:SOAP_MAKER]
        [PERMITTED_BUILDING:SCREW_PRESS]
        [PERMITTED_REACTION:TAN_A_HIDE]
        [PERMITTED_REACTION:RENDER_FAT]
        [WORLD_CONSTRUCTION:TUNNEL]
        [WORLD_CONSTRUCTION:BRIDGE]
        [WORLD_CONSTRUCTION:ROAD]
        [ETHIC:KILL_ENTITY_MEMBER:PUNISH_CAPITAL]
        [ETHIC:KILL_NEUTRAL:ONLY_IF_SANCTIONED]
        [ETHIC:KILL_ENEMY:ACCEPTABLE]
        [VALUE:LAW:50]
        [VALUE:LOYALTY:0]
        [VALUE:FAMILY:-50]
        [WILL_ACCEPT_TRIBUTE]
        [STONE_SHAPE:OVAL_CABOCHON]
        [STONE_SHAPE:ROUND_CABOCHON]
        [GEM_SHAPE:OVAL_CABOCHON]
        [GEM_SHAPE:ROUND_CABOCHON]
    
        [LAND_HOLDER_TRIGGER:1:20:10000:100000]
        [LAND_HOLDER_TRIGGER:2:20:20000:200000]
        [LAND_HOLDER_TRIGGER:3:20:30000:300000]
    
        ---- Positions ----
        [POSITION:MONARCH]
            [NAME_MALE:king:kings]
            [NAME_FEMALE:queen:queens]
            [NUMBER:1]
            [SPOUSE_MALE:king consort:kings consort]
            [SPOUSE_FEMALE:queen consort:queens consort]
            [SUCCESSION:BY_HEIR]
            [RESPONSIBILITY:LAW_MAKING]
            [RESPONSIBILITY:RECEIVE_DIPLOMATS]
            [RESPONSIBILITY:MILITARY_GOALS]
            [PRECEDENCE:1]
            [CHAT_WORTHY]
            [DO_NOT_CULL]
            [KILL_QUEST]
            [EXPORTED_IN_LEGENDS]
            [DETERMINES_COIN_DESIGN]
            [COLOR:5:0:1]
            [ACCOUNT_EXEMPT]
            [DUTY_BOUND]
            [DEMAND_MAX:10]
            [MANDATE_MAX:5]
            [REQUIRED_BOXES:10]
            [REQUIRED_CABINETS:5]
            [REQUIRED_RACKS:5]
            [REQUIRED_STANDS:5]
            [REQUIRED_OFFICE:10000]
            [REQUIRED_BEDROOM:10000]
            [REQUIRED_DINING:10000]
            [REQUIRED_TOMB:10000]
        [POSITION:LIEUTENANT]
            [NAME:lieutenant:lieutenants]
            [NUMBER:AS_NEEDED]
            [SQUAD:10:soldier:soldiers]
            [APPOINTED_BY:MONARCH]
            [COMMANDER:CAPTAIN:ALL]
            [SUCCESSION:BY_POSITION:GENERAL]
            [PRECEDENCE:100]
    
        ---- Tissue Styles ----
        [TISSUE_STYLE:MOUSTACHE]
            [TS_MAINTAIN_LENGTH:100:NONE]
            [TS_PREFERRED_SHAPING:STANDARD_MOUSTACHE_SHAPINGS]
        [TISSUE_STYLE:BEARD]
            [TS_MAINTAIN_LENGTH:100:NONE]
            [TS_PREFERRED_SHAPING:STANDARD_BEARD_SHAPINGS]
        
        ---- Animals ----
        [ANIMAL]
            [ANIMAL_CLASS:CRUSTACEAN]
            [ANIMAL_FORBIDDEN_CLASS:LOBSTER]
            [ANIMAL_FORBIDDEN_CLASS:HERMIT]
            [ANIMAL_NEVER_MOUNT]
        [ANIMAL]
            [ANIMAL_CLASS:REPTILE]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "entity_default".to_owned(),
            object_tokens: vec![ObjectToken {
                entity_tokens: vec![EntityToken {
                    reference: Some(ReferenceTo::new("TEST".to_owned())),
                    animal: vec![
                        Animal {
                            reference: Some(()),
                            animal_class: vec![Reference("CRUSTACEAN".to_owned())],
                            animal_forbidden_class: vec![
                                Reference("LOBSTER".to_owned()),
                                Reference("HERMIT".to_owned()),
                            ],
                            animal_never_mount: Some(()),
                            ..Default::default()
                        },
                        Animal {
                            reference: Some(()),
                            animal_class: vec![Reference("REPTILE".to_owned())],
                            ..Default::default()
                        },
                    ],
                    tissue_style: vec![
                        TissueStyle {
                            reference: Some(ReferenceTo::new("MOUSTACHE".to_owned())),
                            ts_maintain_length: Some((
                                Choose::Choice1(100),
                                Choose::Choice2(NoneEnum::None)
                            )),
                            ts_preferred_shaping: vec![StylingEnum::StandardMoustacheShapings],
                        },
                        TissueStyle {
                            reference: Some(ReferenceTo::new("BEARD".to_owned())),
                            ts_maintain_length: Some((
                                Choose::Choice1(100),
                                Choose::Choice2(NoneEnum::None)
                            )),
                            ts_preferred_shaping: vec![StylingEnum::StandardBeardShapings],
                        },
                    ],
                    all_main_pops_controllable: Some(()),
                    site_controllable: Some(()),
                    creature: vec![
                        ReferenceTo::new("DWARF".to_owned()),
                        ReferenceTo::new("CRAB_MAN".to_owned()),
                    ],
                    biome_support: vec![(BiomeEnum::Mountain, 3), (BiomeEnum::AnyRiver, 1)],
                    settlement_biome: vec![BiomeEnum::AnyForest, BiomeEnum::Mountain],
                    exclusive_start_biome: Some(BiomeEnum::Mountain),
                    default_site_type: Some(SiteTypeEnum::CaveDetailed),
                    likes_site: vec![SiteTypeEnum::CaveDetailed],
                    tolerates_site: vec![SiteTypeEnum::City, SiteTypeEnum::CaveDetailed],
                    world_construction: vec![
                        ConstructionEnum::Tunnel,
                        ConstructionEnum::Bridge,
                        ConstructionEnum::Road
                    ],
                    max_pop_number: Some(10000),
                    max_site_pop_number: Some(120),
                    max_starting_civ_number: Some(100),
                    permitted_building: vec![
                        ReferenceTo::new("SOAP_MAKER".to_owned()),
                        ReferenceTo::new("SCREW_PRESS".to_owned()),
                    ],
                    permitted_job: vec![UnitTypeEnum::Miner, UnitTypeEnum::Carpenter],
                    permitted_reaction: vec![
                        ReferenceTo::new("TAN_A_HIDE".to_owned()),
                        ReferenceTo::new("RENDER_FAT".to_owned()),
                    ],
                    currency_by_year: Some(()),
                    currency: vec![
                        (ReferenceTo::new("COPPER".to_owned()), 1),
                        (ReferenceTo::new("GOLD".to_owned()), 10000),
                    ],
                    item_improvement_modifier: vec![
                        (ItemImprovementModifierEnum::RingsHanging, 64),
                        (ItemImprovementModifierEnum::Spikes, 384)
                    ],
                    translation: Some(ReferenceTo::new("DWARF".to_owned())),
                    select_symbol: vec![
                        (SymbolNounEnum::War, ReferenceTo::new("NAME_WAR".to_owned())),
                        (
                            SymbolNounEnum::Battle,
                            ReferenceTo::new("NAME_BATTLE".to_owned()),
                        ),
                    ],
                    subselect_symbol: vec![
                        (SymbolNounEnum::War, ReferenceTo::new("VIOLENT".to_owned())),
                        (
                            SymbolNounEnum::Battle,
                            ReferenceTo::new("VIOLENT".to_owned())
                        ),
                    ],
                    cull_symbol: vec![
                        (SymbolNounEnum::All, ReferenceTo::new("EVIL".to_owned())),
                        (SymbolNounEnum::All, ReferenceTo::new("FLOWERY".to_owned())),
                    ],
                    friendly_color: Some((1, 0, 1)),
                    religion: Some(ReligionTypeEnum::Pantheon),
                    religion_sphere: vec![SphereEnum::Fortresses, SphereEnum::Oceans],
                    position: vec![
                        EntityPosition {
                            reference: Some(Reference("MONARCH".to_owned())),
                            responsibility: vec![
                                Responsibility {
                                    reference: Some(ResponsibilityEnum::LawMaking),
                                    execution_skill: None,
                                },
                                Responsibility {
                                    reference: Some(ResponsibilityEnum::ReceiveDiplomats),
                                    execution_skill: None,
                                },
                                Responsibility {
                                    reference: Some(ResponsibilityEnum::MilitaryGoals),
                                    execution_skill: None,
                                },
                            ],
                            account_exempt: Some(()),
                            chat_worthy: Some(()),
                            color: Some((5, 0, 1)),
                            demand_max: Some(10),
                            determines_coin_design: Some(()),
                            do_not_cull: Some(()),
                            duty_bound: Some(()),
                            exported_in_legends: Some(()),
                            kill_quest: Some(()),
                            mandate_max: Some(5),
                            name_male: Some(("king".to_owned(), "kings".to_owned())),
                            name_female: Some(("queen".to_owned(), "queens".to_owned())),
                            number: Some(Choose::Choice1(1)),
                            precedence: Some(1),
                            required_bedroom: Some(10000),
                            required_boxes: Some(10),
                            required_cabinets: Some(5),
                            required_dining: Some(10000),
                            required_office: Some(10000),
                            required_racks: Some(5),
                            required_stands: Some(5),
                            required_tomb: Some(10000),
                            spouse_female: Some((
                                "queen consort".to_owned(),
                                "queens consort".to_owned(),
                            )),
                            spouse_male: Some((
                                "king consort".to_owned(),
                                "kings consort".to_owned(),
                            )),
                            succession: Some((SuccessionTypeEnum::ByHeir, None)),
                            ..Default::default()
                        },
                        EntityPosition {
                            reference: Some(Reference("LIEUTENANT".to_owned())),
                            appointed_by: vec![Reference("MONARCH".to_owned())],
                            commander: Some((Reference("CAPTAIN".to_owned()), AllEnum::All)),
                            name: Some(("lieutenant".to_owned(), "lieutenants".to_owned())),
                            number: Some(Choose::Choice2(AsNeededEnum::AsNeeded)),
                            precedence: Some(100),
                            squad: Some((10, "soldier".to_owned(), "soldiers".to_owned())),
                            succession: Some((
                                SuccessionTypeEnum::ByPosition,
                                Some(Reference("GENERAL".to_owned()))
                            ),),
                            ..Default::default()
                        },
                    ],
                    land_holder_trigger: vec![
                        (1, 20, 10000, 100000),
                        (2, 20, 20000, 200000),
                        (3, 20, 30000, 300000),
                    ],
                    ethic: vec![
                        (
                            EthicTypeEnum::KillEntityMember,
                            EthicReactionEnum::PunishCapital
                        ),
                        (
                            EthicTypeEnum::KillNeutral,
                            EthicReactionEnum::OnlyIfSanctioned
                        ),
                        (EthicTypeEnum::KillEnemy, EthicReactionEnum::Acceptable),
                    ],
                    value: vec![
                        (CulturalValueEnum::Law, 50),
                        (CulturalValueEnum::Loyalty, 0),
                        (CulturalValueEnum::Family, -50)
                    ],
                    will_accept_tribute: Some(()),
                    active_season: vec![SeasonEnum::Autumn],
                    sieger: Some(()),
                    scholar: vec![ScholarTypeEnum::Philosopher, ScholarTypeEnum::Mathematician],
                    weapon: vec![
                        Weapon {
                            reference: Some((
                                ReferenceTo::new("ITEM_WEAPON_AXE_BATTLE".to_owned()),
                                Some(RarityEnum::Common),
                            )),
                            ammo: None,
                        },
                        Weapon {
                            reference: Some((
                                ReferenceTo::new("ITEM_WEAPON_CROSSBOW".to_owned()),
                                None,
                            )),
                            ammo: Some((ReferenceTo::new("ITEM_AMMO_BOLTS".to_owned()), None,)),
                        },
                    ],
                    armor: vec![
                        (
                            ReferenceTo::new("ITEM_ARMOR_BREASTPLATE".to_owned()),
                            Some(RarityEnum::Common),
                        ),
                        (
                            ReferenceTo::new("ITEM_ARMOR_MAIL_SHIRT".to_owned()),
                            Some(RarityEnum::Common),
                        ),
                    ],
                    digger: vec![ReferenceTo::new("ITEM_WEAPON_PICK".to_owned())],
                    gloves: vec![
                        (
                            ReferenceTo::new("ITEM_GLOVES_GAUNTLETS".to_owned()),
                            Some(RarityEnum::Common),
                        ),
                        (ReferenceTo::new("ITEM_GLOVES_GLOVES".to_owned()), None,),
                    ],
                    helm: vec![
                        (
                            ReferenceTo::new("ITEM_HELM_HELM".to_owned()),
                            Some(RarityEnum::Common),
                        ),
                        (ReferenceTo::new("ITEM_HELM_CAP".to_owned()), None),
                    ],
                    pants: vec![
                        (
                            ReferenceTo::new("ITEM_PANTS_PANTS".to_owned()),
                            Some(RarityEnum::Common),
                        ),
                        (ReferenceTo::new("ITEM_PANTS_GREAVES".to_owned()), None,),
                    ],
                    shield: vec![
                        ReferenceTo::new("ITEM_SHIELD_SHIELD".to_owned()),
                        ReferenceTo::new("ITEM_SHIELD_BUCKLER".to_owned()),
                    ],
                    shoes: vec![
                        (
                            ReferenceTo::new("ITEM_SHOES_SHOES".to_owned()),
                            Some(RarityEnum::Common),
                        ),
                        (ReferenceTo::new("ITEM_SHOES_BOOTS".to_owned()), None,),
                    ],
                    siegeammo: vec![
                        ReferenceTo::new("ITEM_SIEGEAMMO_BALLISTA".to_owned()),
                        ReferenceTo::new("ITEM_SIEGEAMMO_SHARK_LAZORS".to_owned()),
                    ],
                    toy: vec![
                        ReferenceTo::new("ITEM_TOY_PUZZLEBOX".to_owned()),
                        ReferenceTo::new("ITEM_TOY_BOAT".to_owned()),
                    ],
                    trapcomp: vec![
                        ReferenceTo::new("ITEM_TRAPCOMP_GIANTAXEBLADE".to_owned()),
                        ReferenceTo::new("ITEM_TRAPCOMP_ENORMOUSCORKSCREW".to_owned()),
                    ],
                    clothing: Some(()),
                    subterranean_clothing: Some(()),
                    equipment_improvements: Some(()),
                    gem_shape: vec![
                        ReferenceTo::new("OVAL_CABOCHON".to_owned()),
                        ReferenceTo::new("ROUND_CABOCHON".to_owned()),
                    ],
                    stone_shape: vec![
                        ReferenceTo::new("OVAL_CABOCHON".to_owned()),
                        ReferenceTo::new("ROUND_CABOCHON".to_owned()),
                    ],
                    ..Default::default()
                }],
                ..Default::default()
            }],
        }
    );
}
