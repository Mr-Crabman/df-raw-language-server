use df_ls_core::ReferenceTo;
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_color() {
    let source = "descriptor_color_standard

    [OBJECT:DESCRIPTOR_COLOR]
    
    Simple test
    
    [COLOR:AMBER]
        [NAME:amber]
        [WORD:AMBER]
        [RGB:255:191:0]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "descriptor_color_standard".to_owned(),
            object_tokens: vec![ObjectToken {
                color_tokens: vec![ColorToken {
                    reference: Some(ReferenceTo::new("AMBER".to_owned())),
                    name: Some("amber".to_owned()),
                    word: vec![ReferenceTo::new("AMBER".to_owned())],
                    rgb: Some((255, 191, 0)),
                },],
                ..Default::default()
            },],
        }
    );
}
