mod common;
use df_ls_core::ReferenceTo;
use df_ls_diagnostics::lsp_types::*;
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_pattern() {
    let source = "descriptor_pattern_iris_eye

    [OBJECT:DESCRIPTOR_PATTERN]
    
    [COLOR_PATTERN:IRIS_EYE_AMBER]
        [PATTERN:IRIS_EYE]
        [CP_COLOR:WHITE]
        [CP_COLOR:BLACK]
        [CP_COLOR:AMBER]
    
    [COLOR_PATTERN:PUPIL_EYE_AMBER]
        [PATTERN:PUPIL_EYE]
        [CP_COLOR:BLACK]
        [CP_COLOR:AMBER]
    
    [COLOR_PATTERN:SPOTS_ORANGE_BLACK]
        [PATTERN:SPOTS]
        [CP_COLOR:ORANGE]
        [CP_COLOR:BLACK]
    
    [COLOR_PATTERN:WRONG]
        [PATTERN:SPO]
        [CP_COLOR:ORANGE]
        [CP_COLOR:BLACK]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        DFRaw {
            header: "descriptor_pattern_iris_eye".to_owned(),
            object_tokens: vec![ObjectToken {
                pattern_tokens: vec![
                    PatternToken {
                        reference: Some(ReferenceTo::new("IRIS_EYE_AMBER".to_owned())),
                        pattern: Some(PatternEnum::IrisEye),
                        cp_color: vec![
                            ReferenceTo::new("WHITE".to_owned()),
                            ReferenceTo::new("BLACK".to_owned()),
                            ReferenceTo::new("AMBER".to_owned()),
                        ],
                    },
                    PatternToken {
                        reference: Some(ReferenceTo::new("PUPIL_EYE_AMBER".to_owned())),
                        pattern: Some(PatternEnum::PupilEye),
                        cp_color: vec![
                            ReferenceTo::new("BLACK".to_owned()),
                            ReferenceTo::new("AMBER".to_owned()),
                        ],
                    },
                    PatternToken {
                        reference: Some(ReferenceTo::new("SPOTS_ORANGE_BLACK".to_owned())),
                        pattern: Some(PatternEnum::Spots),
                        cp_color: vec![
                            ReferenceTo::new("ORANGE".to_owned()),
                            ReferenceTo::new("BLACK".to_owned()),
                        ],
                    },
                    PatternToken {
                        reference: Some(ReferenceTo::new("WRONG".to_owned())),
                        pattern: None,
                        cp_color: vec![
                            ReferenceTo::new("ORANGE".to_owned()),
                            ReferenceTo::new("BLACK".to_owned()),
                        ],
                    },
                ],
                ..Default::default()
            },],
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_enum_value".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 21,
                character: 17,
            },
            end: Position {
                line: 21,
                character: 20,
            },
        }],
    );
}
