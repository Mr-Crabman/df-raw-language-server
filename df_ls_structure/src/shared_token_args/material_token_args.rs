use crate::{
    CreatureToken, InorganicToken, NoMatGlossEnum, NoneEnum, PlantToken, ReactionToken,
    ReagentToken,
};
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_syntax_analysis::TokenDeserialize;
use df_ls_syntax_analysis::{
    Argument, LoopControl, Token, TokenValue, TreeCursor, TryFromArgument,
};
use serde::{Deserialize, Serialize};

/// Wiki page: https://dwarffortresswiki.org/index.php/Material_token
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct MaterialTokenArg {
    // |---0------|---------1--------|----2----|---3-----|
    // [TOKEN_NAME:LOCAL_CREATURE_MAT          :MUSCLE   ]
    // [TOKEN_NAME:PLANT_MAT         :APPLE    :FRUIT    ]
    // [TOKEN_NAME:CREATURE_MAT      :ANIMAL   :WOOL     ]
    // [TOKEN_NAME:INORGANIC                   :QUICKLIME]
    /// Argument group 1: with Enum arguments
    pub material: MaterialTypeEnum,
}

impl MaterialTokenArg {
    pub fn arguments_to_material_token_arg(
        arg0_option: Option<&Argument>,
        arg1_option: Option<&Argument>,
        arg2_option: Option<&Argument>,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(MaterialTokenArg, usize), ()> {
        // Argument group 1
        let arg0 = match arg0_option {
            Some(v) => Ok(v),
            None => Err(()),
        }?;
        if let TokenValue::TVReference(v) = &arg0.value {
            let (material_type, argument_consumed) = match v.as_ref() {
                "INORGANIC" | "STONE" | "METAL" => {
                    // TODO: add alias warning
                    let material_name = ReferenceTo::<InorganicToken>::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Inorganic(material_name), 2)
                }
                "CREATURE_MAT" => {
                    let material_object_id = ReferenceTo::<CreatureToken>::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    let material_name = Reference::try_from_argument(
                        arg2_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (
                        MaterialTypeEnum::CreatureMat((material_object_id, material_name)),
                        3,
                    )
                }
                "LOCAL_CREATURE_MAT" => {
                    let material_name = Reference::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::LocalCreatureMat(material_name), 2)
                }
                "PLANT_MAT" => {
                    let material_object_id = ReferenceTo::<PlantToken>::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    let material_name = Reference::try_from_argument(
                        arg2_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (
                        MaterialTypeEnum::PlantMat((material_object_id, material_name)),
                        3,
                    )
                }
                "LOCAL_PLANT_MAT" => {
                    let material_name = Reference::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::LocalPlantMat(material_name), 2)
                }
                "GET_MATERIAL_FROM_REAGENT" => {
                    let material_object_id = ReferenceTo::<ReactionToken>::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    let material_name =
                        Choose::<NoneEnum, ReferenceTo<ReagentToken>>::try_from_argument(
                            arg2_option,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                    (
                        MaterialTypeEnum::GetMaterialFromReagent((
                            material_object_id,
                            material_name,
                        )),
                        3,
                    )
                }
                "AMBER" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Amber(material_name), 2)
                }
                "CORAL" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Coral(material_name), 2)
                }
                "GLASS_GREEN" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::GlassGreen(material_name), 2)
                }
                "GLASS_CLEAR" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::GlassClear(material_name), 2)
                }
                "GLASS_CRYSTAL" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::GlassCrystal(material_name), 2)
                }
                "WATER" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Water(material_name), 2)
                }
                "COAL" => {
                    let material_name = CoalMaterialEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Coal(material_name), 2)
                }
                "POTASH" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Potash(material_name), 2)
                }
                "LYE" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Lye(material_name), 2)
                }
                "ASH" => {
                    let material_name = Choose::<NoneEnum, NoMatGlossEnum>::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Ash(material_name), 2)
                }
                "PEARLASH" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Pearlash(material_name), 2)
                }
                "MUD" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Mud(material_name), 2)
                }
                "VOMIT" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Vomit(material_name), 2)
                }
                "SALT" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Salt(material_name), 2)
                }
                "FILTH_B" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::FilthB(material_name), 2)
                }
                "FILTH_Y" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::FilthY(material_name), 2)
                }
                "UNKNOWN_SUBSTANCE" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::UnknownSubstance(material_name), 2)
                }
                "GRIME" => {
                    let material_name = NoneEnum::try_from_argument(
                        arg1_option,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    (MaterialTypeEnum::Grime(material_name), 2)
                }
                _ => {
                    return Err(());
                }
            };
            return Ok((
                MaterialTokenArg {
                    material: material_type,
                },
                argument_consumed,
            ));
        } else if add_diagnostics_on_err {
            diagnostics.add_message(
                DMExtraInfo {
                    range: arg0.node.get_range(),
                    message_template_data: hash_map! {
                        "expected_parameters" => Reference::expected_argument_types(),
                        "found_parameters" => TokenValue::argument_to_token_name(&arg0.value),
                    },
                },
                "wrong_arg_type",
            );
        }
        Err(())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
/// The shape of the tissue
pub enum MaterialTypeEnum {
    /// Specifies a standalone inorganic material defined in the raws, generally a stone or metal.
    /// For example, `INORGANIC:IRON` refers to iron, and `INORGANIC:CERAMIC_PORCELAIN`
    /// refers to porcelain. The material name can be substituted with `USE_LAVA_STONE`
    /// to automatically select the local lava stone, which is normally obsidian.
    // #[token_de(token = "INORGANIC", alias = "STONE", alias = "METAL")]
    Inorganic(ReferenceTo<InorganicToken>),
    /// Specifies a material associated with a specific creature.
    /// Examples: `CREATURE_MAT:DWARF:SKIN` refers to dwarf skin.
    // #[token_de(token = "CREATURE_MAT")]
    CreatureMat((ReferenceTo<CreatureToken>, Reference)),
    /// Alias for `CREATURE_MAT:CREATURE_ID:MATERIAL_NAME`,
    /// where `CREATURE_ID` is the creature currently being defined;
    /// as such, it can only be used in creature definitions.
    // #[token_de(token = "LOCAL_CREATURE_MAT")]
    LocalCreatureMat(Reference),
    /// Specifies a material associated with a specific plant.
    /// Example: `PLANT_MAT:BUSH_QUARRY:LEAF` refers to quarry bush leaves.
    // #[token_de(token = "PLANT_MAT")]
    PlantMat((ReferenceTo<PlantToken>, Reference)),
    /// Alias for `PLANT_MAT:PLANT_ID:MATERIAL_NAME`,
    /// where `PLANT_ID` is the plant currently being defined;
    /// as such, it can only be used in plant definitions.
    // #[token_de(token = "LOCAL_PLANT_MAT")]
    LocalPlantMat(Reference),
    /// Specifies a material related to a reagent's material within a reaction.
    /// `REAGENT_ID` must match a `[REAGENT]`, and `REACTION_PRODUCT_ID` must either match a
    /// `[MATERIAL_REACTION_PRODUCT]` belonging to the reagent's material
    /// or be equal to `NONE` to use the reagent's material itself.
    // #[token_de(token = "GET_MATERIAL_FROM_REAGENT")]
    GetMaterialFromReagent(
        (
            ReferenceTo<ReactionToken>,
            Choose<NoneEnum, ReferenceTo<ReagentToken>>,
        ),
    ),
    //-----------Hardcoded materials--------
    /// Specifies one of the hardcoded materials.
    /// Amber is a type of material made from fossilized tree resin.
    // #[token_de(token = "AMBER")]
    Amber(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Coral is a type of material composed of the dead remains of corals,
    /// creatures that have not yet been implemented into the game.
    // #[token_de(token = "CORAL")]
    Coral(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[token_de(token = "GLASS_GREEN")]
    GlassGreen(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[token_de(token = "GLASS_CLEAR")]
    GlassClear(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[token_de(token = "GLASS_CRYSTAL")]
    GlassCrystal(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Water, when placed in buckets or when mining out ice.
    // #[token_de(token = "WATER")]
    Water(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Specifies a material that can be used as fuel - charcoal or coke.
    /// Specifying `NO_MATGLOSS` (not `NONE`) will make it accept "refined coal" in general,
    /// which matches charcoal, coke, and generic refined coal.
    // #[token_de(token = "COAL")]
    Coal(CoalMaterialEnum),
    /// Specifies one of the hardcoded materials.
    /// Potash is a wood-based product which has applications in farming,
    /// as well as production of mid- and high-end glass products.
    // #[token_de(token = "POTASH")]
    Potash(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Ash is an intermediate good used to make potash, lye, or to glaze ceramics.
    // #[token_de(token = "ASH")]
    Ash(Choose<NoneEnum, NoMatGlossEnum>),
    /// Specifies one of the hardcoded materials.
    /// Pearlash is a wood-based product which is used primarily
    /// in the manufacture of clear and crystal glass.
    // #[token_de(token = "PEARLASH")]
    Pearlash(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Lye is a material used to make soap, and can also be used to make potash.
    // #[token_de(token = "LYE")]
    Lye(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Mud is a contaminant produced when an area is covered with water, and colors tiles brown.
    // #[token_de(token = "MUD")]
    Mud(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Under certain conditions, creatures (such as your dwarves) will vomit,
    /// creating a puddle of vomit.
    // #[token_de(token = "VOMIT")]
    Vomit(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Salt is a contaminant that makes oceanic water unsuitable for drinking.
    // #[token_de(token = "SALT")]
    Salt(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Filth comes in two varieties: solid brown (B) and liquid yellow (Y) filth.
    // #[token_de(token = "FILTH_B")]
    FilthB(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Filth comes in two varieties: solid brown (B) and liquid yellow (Y) filth.
    // #[token_de(token = "FILTH_Y")]
    FilthY(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Unknown substance is a hardcoded material that takes the form of a light gray liquid.
    /// No longer used in recent versions of game.
    // #[token_de(token = "UNKNOWN_SUBSTANCE")]
    UnknownSubstance(NoneEnum),
    /// Specifies one of the hardcoded materials.
    /// Grime is a brown-colored contaminant that makes water from murky pools disgusting to drink.
    // #[token_de(token = "GRIME")]
    Grime(NoneEnum),
}
impl Default for MaterialTypeEnum {
    fn default() -> Self {
        Self::Inorganic(ReferenceTo::new(String::default()))
    }
}

impl TokenDeserialize for MaterialTokenArg {
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        token.check_token_arg0(&source, &mut diagnostics, true)?;
        token.check_arguments_length_fixed((2, 3), &source, &mut diagnostics, true)?;

        let (result, _arguments_consumption) = MaterialTokenArg::arguments_to_material_token_arg(
            token.arguments.get(1),
            token.arguments.get(2),
            token.arguments.get(3),
            source,
            &mut diagnostics,
            true,
        )?;
        Ok(result)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum CoalMaterialEnum {
    #[token_de(token = "CHARCOAL")]
    Charcoal,
    #[token_de(token = "COKE")]
    Coke,
    /// Make it accept "refined coal" in general,
    /// which matches charcoal, coke, and generic refined coal.
    #[token_de(token = "NO_MATGLOSS")]
    NoMatgloss,
}
impl Default for CoalMaterialEnum {
    fn default() -> Self {
        Self::Charcoal
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_material_correct() {
        let source = "header
            [REF:INORGANIC:IRON]
            [REF:STONE:SLADE]
            [REF:METAL:IRON]
            [REF:CREATURE_MAT:HONEY_BEE:HONEY]
            [REF:LOCAL_CREATURE_MAT:SKIN]
            [REF:PLANT_MAT:NETHER_CAP:WOOD]
            [REF:LOCAL_PLANT_MAT:LEAF]
            [REF:GET_MATERIAL_FROM_REAGENT:PLANT:PRESS_PAPER_MAT]
            [REF:GET_MATERIAL_FROM_REAGENT:BONE:NONE]
            [REF:AMBER:NONE]
            TISSUE_MATERIAL:AMBER // Is this valid? Exists in `masterwork_df_v1.31/objects/creature_civ_titan.txt`
            [REF:CORAL:NONE]
            [REF:GLASS_GREEN:NONE]
            TISSUE_MATERIAL:GLASS_GREEN // Is this valid?
            [REF:GLASS_CLEAR:NONE]
            [REF:GLASS_CRYSTAL:NONE]
            [REF:WATER:NONE]
            TISSUE_MATERIAL:WATER // Is this valid?
            [REF:COAL:CHARCOAL]
            [REF:COAL:COKE]
            [REF:COAL:NO_MATGLOSS]
            [REF:POTASH:NONE]
            [REF:ASH:NONE] // Both look to be valid judging for RAW collection
            [REF:ASH:NO_MATGLOSS] // Both look to be valid judging for RAW collection
            [REF:PEARLASH:NONE]
            REF:LYE // Is this valid?
            [REF:LYE:NONE]
            [REF:MUD:NONE]
            REF:MUD // Is this valid?
            [REF:VOMIT:NONE]
            REF:VOMIT // Is this valid?
            [REF:SALT:NONE]
            [REF:FILTH_B:NONE]
            [REF:FILTH_Y:NONE]
            [REF:UNKNOWN_SUBSTANCE:NONE]
            [REF:GRIME:NONE]
            ";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test `[REF:INORGANIC:IRON]` ---
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Inorganic(ReferenceTo::new("IRON".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:STONE:SLADE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Inorganic(ReferenceTo::new("SLADE".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:METAL:IRON]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Inorganic(ReferenceTo::new("IRON".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:CREATURE_MAT:HONEY_BEE:HONEY]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::CreatureMat((
                    ReferenceTo::new("HONEY_BEE".to_owned()),
                    Reference("HONEY".to_owned())
                )),
            },
            test_result
        );

        // ---- Test `[REF:LOCAL_CREATURE_MAT:SKIN]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::LocalCreatureMat(Reference("SKIN".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:PLANT_MAT:NETHER_CAP:WOOD]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::PlantMat((
                    ReferenceTo::new("NETHER_CAP".to_owned()),
                    Reference("WOOD".to_owned())
                )),
            },
            test_result
        );

        // ---- Test `[REF:LOCAL_PLANT_MAT:LEAF]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::LocalPlantMat(Reference("LEAF".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:GET_MATERIAL_FROM_REAGENT:PLANT:PRESS_PAPER_MAT]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GetMaterialFromReagent((
                    ReferenceTo::new("PLANT".to_owned()),
                    Choose::Choice2(ReferenceTo::new("PRESS_PAPER_MAT".to_owned()))
                )),
            },
            test_result
        );

        // ---- Test `[REF:GET_MATERIAL_FROM_REAGENT:BONE:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GetMaterialFromReagent((
                    ReferenceTo::new("BONE".to_owned()),
                    Choose::Choice1(NoneEnum::None)
                )),
            },
            test_result
        );

        // ---- Test `[REF:AMBER:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Amber(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:CORAL:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coral(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_GREEN:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassGreen(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_CLEAR:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassClear(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_CRYSTAL:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassCrystal(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:WATER:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Water(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:COAL:CHARCOAL]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coal(CoalMaterialEnum::Charcoal),
            },
            test_result
        );

        // ---- Test `[REF:COAL:COKE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coal(CoalMaterialEnum::Coke),
            },
            test_result
        );

        // ---- Test `[REF:COAL:NO_MATGLOSS]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coal(CoalMaterialEnum::NoMatgloss),
            },
            test_result
        );

        // ---- Test `[REF:POTASH:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Potash(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:ASH:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Ash(Choose::Choice1(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:ASH:NO_MATGLOSS]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Ash(Choose::Choice2(NoMatGlossEnum::NoMatgloss)),
            },
            test_result
        );

        // ---- Test `[REF:PEARLASH:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Pearlash(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:LYE:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Lye(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:MUD:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Mud(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:VOMIT:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Vomit(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:SALT:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Salt(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:FILTH_B:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::FilthB(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:FILTH_Y:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::FilthY(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:UNKNOWN_SUBSTANCE:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::UnknownSubstance(NoneEnum::None),
            },
            test_result
        );

        // ---- Test `[REF:GRIME:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Grime(NoneEnum::None),
            },
            test_result
        );

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
