use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum WeaponSkillEnum {
    #[token_de(token = "AXE")]
    Axe,
    #[token_de(token = "SWORD")]
    Sword,
    #[token_de(token = "DAGGER")]
    Dagger,
    #[token_de(token = "MACE")]
    Mace,
    #[token_de(token = "HAMMER")]
    Hammer,
    #[token_de(token = "SPEAR")]
    Spear,
    #[token_de(token = "CROSSBOW")]
    Crossbow,
    #[token_de(token = "SHIELD")]
    Shield,
    #[token_de(token = "PIKE")]
    Pike,
    #[token_de(token = "WHIP")]
    Whip,
    #[token_de(token = "BOW")]
    Bow,
    #[token_de(token = "BLOWGUN")]
    Blowgun,
}
impl Default for WeaponSkillEnum {
    fn default() -> Self {
        Self::Axe
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum MusicSkillEnum {
    #[token_de(token = "PLAY_KEYBOARD_INSTRUMENT")]
    PlayKeyboardInstrument,
    #[token_de(token = "PLAY_STRINGED_INSTRUMENT")]
    PlayStringedInstrument,
    #[token_de(token = "PLAY_WIND_INSTRUMENT")]
    PlayWindInstrument,
    #[token_de(token = "PLAY_PERCUSSION_INSTRUMENT")]
    PlayPercussionInstrument,
}
impl Default for MusicSkillEnum {
    fn default() -> Self {
        Self::PlayKeyboardInstrument
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum SkillEnum {
    #[token_de(token = "MINING")]
    Mining,
    #[token_de(token = "WOODCUTTING")]
    Woodcutting,
    #[token_de(token = "CARPENTRY")]
    Carpentry,
    #[token_de(token = "DETAILSTONE")]
    Detailstone,
    #[token_de(token = "MASONRY")]
    Masonry,
    #[token_de(token = "ANIMALTRAIN")]
    Animaltrain,
    #[token_de(token = "ANIMALCARE")]
    Animalcare,
    #[token_de(token = "DISSECT_FISH")]
    DissectFish,
    #[token_de(token = "DISSECT_VERMIN")]
    DissectVermin,
    #[token_de(token = "PROCESSFISH")]
    Processfish,
    #[token_de(token = "BUTCHER")]
    Butcher,
    #[token_de(token = "TRAPPING")]
    Trapping,
    #[token_de(token = "TANNER")]
    Tanner,
    #[token_de(token = "WEAVING")]
    Weaving,
    #[token_de(token = "BREWING")]
    Brewing,
    #[token_de(token = "ALCHEMY")]
    Alchemy,
    #[token_de(token = "CLOTHESMAKING")]
    Clothesmaking,
    #[token_de(token = "MILLING")]
    Milling,
    #[token_de(token = "PROCESSPLANTS")]
    Processplants,
    #[token_de(token = "CHEESEMAKING")]
    Cheesemaking,
    #[token_de(token = "MILK")]
    Milk,
    #[token_de(token = "COOK")]
    Cook,
    #[token_de(token = "PLANT")]
    Plant,
    #[token_de(token = "HERBALISM")]
    Herbalism,
    #[token_de(token = "FISH")]
    Fish,
    #[token_de(token = "SMELT")]
    Smelt,
    #[token_de(token = "EXTRACT_STRAND")]
    ExtractStrand,
    #[token_de(token = "FORGE_WEAPON")]
    ForgeWeapon,
    #[token_de(token = "FORGE_ARMOR")]
    ForgeArmor,
    #[token_de(token = "FORGE_FURNITURE")]
    ForgeFurniture,
    #[token_de(token = "CUTGEM")]
    Cutgem,
    #[token_de(token = "ENCRUSTGEM")]
    Encrustgem,
    #[token_de(token = "WOODCRAFT")]
    Woodcraft,
    #[token_de(token = "STONECRAFT")]
    Stonecraft,
    #[token_de(token = "METALCRAFT")]
    Metalcraft,
    #[token_de(token = "GLASSMAKER")]
    Glassmaker,
    #[token_de(token = "LEATHERWORK")]
    Leatherwork,
    #[token_de(token = "BONECARVE")]
    Bonecarve,
    #[token_de(token = "AXE")]
    Axe,
    #[token_de(token = "SWORD")]
    Sword,
    #[token_de(token = "DAGGER")]
    Dagger,
    #[token_de(token = "MACE")]
    Mace,
    #[token_de(token = "HAMMER")]
    Hammer,
    #[token_de(token = "SPEAR")]
    Spear,
    #[token_de(token = "CROSSBOW")]
    Crossbow,
    #[token_de(token = "SHIELD")]
    Shield,
    #[token_de(token = "ARMOR")]
    Armor,
    #[token_de(token = "SIEGECRAFT")]
    Siegecraft,
    #[token_de(token = "SIEGEOPERATE")]
    Siegeoperate,
    #[token_de(token = "BOWYER")]
    Bowyer,
    #[token_de(token = "PIKE")]
    Pike,
    #[token_de(token = "WHIP")]
    Whip,
    #[token_de(token = "BOW")]
    Bow,
    #[token_de(token = "BLOWGUN")]
    Blowgun,
    #[token_de(token = "THROW")]
    Throw,
    #[token_de(token = "MECHANICS")]
    Mechanics,
    #[token_de(token = "MAGIC_NATURE")]
    MagicNature,
    #[token_de(token = "SNEAK")]
    Sneak,
    #[token_de(token = "DESIGNBUILDING")]
    Designbuilding,
    #[token_de(token = "DRESS_WOUNDS")]
    DressWounds,
    #[token_de(token = "DIAGNOSE")]
    Diagnose,
    #[token_de(token = "SURGERY")]
    Surgery,
    #[token_de(token = "SET_BONE")]
    SetBone,
    #[token_de(token = "SUTURE")]
    Suture,
    #[token_de(token = "CRUTCH_WALK")]
    CrutchWalk,
    #[token_de(token = "WOOD_BURNING")]
    WoodBurning,
    #[token_de(token = "LYE_MAKING")]
    LyeMaking,
    #[token_de(token = "SOAP_MAKING")]
    SoapMaking,
    #[token_de(token = "POTASH_MAKING")]
    PotashMaking,
    #[token_de(token = "DYER")]
    Dyer,
    #[token_de(token = "OPERATE_PUMP")]
    OperatePump,
    #[token_de(token = "SWIMMING")]
    Swimming,
    #[token_de(token = "PERSUASION")]
    Persuasion,
    #[token_de(token = "NEGOTIATION")]
    Negotiation,
    #[token_de(token = "JUDGING_INTENT")]
    JudgingIntent,
    #[token_de(token = "APPRAISAL")]
    Appraisal,
    #[token_de(token = "ORGANIZATION")]
    Organization,
    #[token_de(token = "RECORD_KEEPING")]
    RecordKeeping,
    #[token_de(token = "LYING")]
    Lying,
    #[token_de(token = "INTIMIDATION")]
    Intimidation,
    #[token_de(token = "CONVERSATION")]
    Conversation,
    #[token_de(token = "COMEDY")]
    Comedy,
    #[token_de(token = "FLATTERY")]
    Flattery,
    #[token_de(token = "CONSOLE")]
    Console,
    #[token_de(token = "PACIFY")]
    Pacify,
    #[token_de(token = "TRACKING")]
    Tracking,
    #[token_de(token = "KNOWLEDGE_ACQUISITION")]
    KnowledgeAcquisition,
    #[token_de(token = "CONCENTRATION")]
    Concentration,
    #[token_de(token = "DISCIPLINE")]
    Discipline,
    #[token_de(token = "SITUATIONAL_AWARENESS")]
    SituationalAwareness,
    #[token_de(token = "WRITING")]
    Writing,
    #[token_de(token = "PROSE")]
    Prose,
    #[token_de(token = "POETRY")]
    Poetry,
    #[token_de(token = "READING")]
    Reading,
    #[token_de(token = "SPEAKING")]
    Speaking,
    #[token_de(token = "COORDINATION")]
    Coordination,
    #[token_de(token = "BALANCE")]
    Balance,
    #[token_de(token = "LEADERSHIP")]
    Leadership,
    #[token_de(token = "TEACHING")]
    Teaching,
    #[token_de(token = "MELEE_COMBAT")]
    MeleeCombat,
    #[token_de(token = "RANGED_COMBAT")]
    RangedCombat,
    #[token_de(token = "WRESTLING")]
    Wrestling,
    #[token_de(token = "BITE")]
    Bite,
    #[token_de(token = "GRASP_STRIKE")]
    GraspStrike,
    #[token_de(token = "STANCE_STRIKE")]
    StanceStrike,
    #[token_de(token = "DODGING")]
    Dodging,
    #[token_de(token = "MISC_WEAPON")]
    MiscWeapon,
    #[token_de(token = "KNAPPING")]
    Knapping,
    #[token_de(token = "MILITARY_TACTICS")]
    MilitaryTactics,
    #[token_de(token = "SHEARING")]
    Shearing,
    #[token_de(token = "SPINNING")]
    Spinning,
    #[token_de(token = "POTTERY")]
    Pottery,
    #[token_de(token = "GLAZING")]
    Glazing,
    #[token_de(token = "PRESSING")]
    Pressing,
    #[token_de(token = "BEEKEEPING")]
    Beekeeping,
    #[token_de(token = "WAX_WORKING")]
    WaxWorking,
    #[token_de(token = "CLIMBING")]
    Climbing,
    #[token_de(token = "GELD")]
    Geld,
    #[token_de(token = "DANCE")]
    Dance,
    #[token_de(token = "MAKE_MUSIC")]
    MakeMusic,
    #[token_de(token = "SING")]
    Sing,
    #[token_de(token = "PLAY_KEYBOARD_INSTRUMENT")]
    PlayKeyboardInstrument,
    #[token_de(token = "PLAY_STRINGED_INSTRUMENT")]
    PlayStringedInstrument,
    #[token_de(token = "PLAY_WIND_INSTRUMENT")]
    PlayWindInstrument,
    #[token_de(token = "PLAY_PERCUSSION_INSTRUMENT")]
    PlayPercussionInstrument,
    #[token_de(token = "CRITICAL_THINKING")]
    CriticalThinking,
    #[token_de(token = "LOGIC")]
    Logic,
    #[token_de(token = "MATHEMATICS")]
    Mathematics,
    #[token_de(token = "ASTRONOMY")]
    Astronomy,
    #[token_de(token = "CHEMISTRY")]
    Chemistry,
    #[token_de(token = "GEOGRAPHY")]
    Geography,
    #[token_de(token = "OPTICS_ENGINEER")]
    OpticsEngineer,
    #[token_de(token = "FLUID_ENGINEER")]
    FluidEngineer,
    #[token_de(token = "PAPERMAKING")]
    Papermaking,
    #[token_de(token = "BOOKBINDING")]
    Bookbinding,
    #[token_de(token = "INTRIGUE")]
    Intrigue,
    #[token_de(token = "RIDING")]
    Riding,
}
impl Default for SkillEnum {
    fn default() -> Self {
        Self::Mining
    }
}
