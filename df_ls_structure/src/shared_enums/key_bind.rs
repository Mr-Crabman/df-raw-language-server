use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
/// For more key binds see: issue #76
pub enum KeyBindEnum {
    #[token_de(token = "HOTKEY_STILL_BREW")]
    HotkeyStillBrew,
    #[token_de(token = "HOTKEY_KITCHEN_RENDER_FAT")]
    HotkeyKitchenRenderFat,
    #[token_de(token = "CUSTOM_A")]
    CustomA,
    #[token_de(token = "CUSTOM_B")]
    CustomB,
    #[token_de(token = "CUSTOM_C")]
    CustomC,
    #[token_de(token = "CUSTOM_D")]
    CustomD,
    #[token_de(token = "CUSTOM_E")]
    CustomE,
    #[token_de(token = "CUSTOM_F")]
    CustomF,
    #[token_de(token = "CUSTOM_G")]
    CustomG,
    #[token_de(token = "CUSTOM_H")]
    CustomH,
    #[token_de(token = "CUSTOM_I")]
    CustomI,
    #[token_de(token = "CUSTOM_J")]
    CustomJ,
    #[token_de(token = "CUSTOM_K")]
    CustomK,
    #[token_de(token = "CUSTOM_L")]
    CustomL,
    #[token_de(token = "CUSTOM_M")]
    CustomM,
    #[token_de(token = "CUSTOM_N")]
    CustomN,
    #[token_de(token = "CUSTOM_O")]
    CustomO,
    #[token_de(token = "CUSTOM_P")]
    CustomP,
    #[token_de(token = "CUSTOM_Q")]
    CustomQ,
    #[token_de(token = "CUSTOM_R")]
    CustomR,
    #[token_de(token = "CUSTOM_S")]
    CustomS,
    #[token_de(token = "CUSTOM_T")]
    CustomT,
    #[token_de(token = "CUSTOM_U")]
    CustomU,
    #[token_de(token = "CUSTOM_V")]
    CustomV,
    #[token_de(token = "CUSTOM_W")]
    CustomW,
    #[token_de(token = "CUSTOM_X")]
    CustomX,
    #[token_de(token = "CUSTOM_Y")]
    CustomY,
    #[token_de(token = "CUSTOM_Z")]
    CustomZ,
    #[token_de(token = "CUSTOM_SHIFT_A")]
    CustomShiftA,
    #[token_de(token = "CUSTOM_SHIFT_B")]
    CustomShiftB,
    #[token_de(token = "CUSTOM_SHIFT_C")]
    CustomShiftC,
    #[token_de(token = "CUSTOM_SHIFT_D")]
    CustomShiftD,
    #[token_de(token = "CUSTOM_SHIFT_E")]
    CustomShiftE,
    #[token_de(token = "CUSTOM_SHIFT_F")]
    CustomShiftF,
    #[token_de(token = "CUSTOM_SHIFT_G")]
    CustomShiftG,
    #[token_de(token = "CUSTOM_SHIFT_H")]
    CustomShiftH,
    #[token_de(token = "CUSTOM_SHIFT_I")]
    CustomShiftI,
    #[token_de(token = "CUSTOM_SHIFT_J")]
    CustomShiftJ,
    #[token_de(token = "CUSTOM_SHIFT_K")]
    CustomShiftK,
    #[token_de(token = "CUSTOM_SHIFT_L")]
    CustomShiftL,
    #[token_de(token = "CUSTOM_SHIFT_M")]
    CustomShiftM,
    #[token_de(token = "CUSTOM_SHIFT_N")]
    CustomShiftN,
    #[token_de(token = "CUSTOM_SHIFT_O")]
    CustomShiftO,
    #[token_de(token = "CUSTOM_SHIFT_P")]
    CustomShiftP,
    #[token_de(token = "CUSTOM_SHIFT_Q")]
    CustomShiftQ,
    #[token_de(token = "CUSTOM_SHIFT_R")]
    CustomShiftR,
    #[token_de(token = "CUSTOM_SHIFT_S")]
    CustomShiftS,
    #[token_de(token = "CUSTOM_SHIFT_T")]
    CustomShiftT,
    #[token_de(token = "CUSTOM_SHIFT_U")]
    CustomShiftU,
    #[token_de(token = "CUSTOM_SHIFT_V")]
    CustomShiftV,
    #[token_de(token = "CUSTOM_SHIFT_W")]
    CustomShiftW,
    #[token_de(token = "CUSTOM_SHIFT_X")]
    CustomShiftX,
    #[token_de(token = "CUSTOM_SHIFT_Y")]
    CustomShiftY,
    #[token_de(token = "CUSTOM_SHIFT_Z")]
    CustomShiftZ,
    #[token_de(token = "CUSTOM_CTRL_A")]
    CustomCtrlA,
    #[token_de(token = "CUSTOM_CTRL_B")]
    CustomCtrlB,
    #[token_de(token = "CUSTOM_CTRL_C")]
    CustomCtrlC,
    #[token_de(token = "CUSTOM_CTRL_D")]
    CustomCtrlD,
    #[token_de(token = "CUSTOM_CTRL_E")]
    CustomCtrlE,
    #[token_de(token = "CUSTOM_CTRL_F")]
    CustomCtrlF,
    #[token_de(token = "CUSTOM_CTRL_G")]
    CustomCtrlG,
    #[token_de(token = "CUSTOM_CTRL_H")]
    CustomCtrlH,
    #[token_de(token = "CUSTOM_CTRL_I")]
    CustomCtrlI,
    #[token_de(token = "CUSTOM_CTRL_J")]
    CustomCtrlJ,
    #[token_de(token = "CUSTOM_CTRL_K")]
    CustomCtrlK,
    #[token_de(token = "CUSTOM_CTRL_L")]
    CustomCtrlL,
    #[token_de(token = "CUSTOM_CTRL_M")]
    CustomCtrlM,
    #[token_de(token = "CUSTOM_CTRL_N")]
    CustomCtrlN,
    #[token_de(token = "CUSTOM_CTRL_O")]
    CustomCtrlO,
    #[token_de(token = "CUSTOM_CTRL_P")]
    CustomCtrlP,
    #[token_de(token = "CUSTOM_CTRL_Q")]
    CustomCtrlQ,
    #[token_de(token = "CUSTOM_CTRL_R")]
    CustomCtrlR,
    #[token_de(token = "CUSTOM_CTRL_S")]
    CustomCtrlS,
    #[token_de(token = "CUSTOM_CTRL_T")]
    CustomCtrlT,
    #[token_de(token = "CUSTOM_CTRL_U")]
    CustomCtrlU,
    #[token_de(token = "CUSTOM_CTRL_V")]
    CustomCtrlV,
    #[token_de(token = "CUSTOM_CTRL_W")]
    CustomCtrlW,
    #[token_de(token = "CUSTOM_CTRL_X")]
    CustomCtrlX,
    #[token_de(token = "CUSTOM_CTRL_Y")]
    CustomCtrlY,
    #[token_de(token = "CUSTOM_CTRL_Z")]
    CustomCtrlZ,
    #[token_de(token = "CUSTOM_ALT_A")]
    CustomAltA,
    #[token_de(token = "CUSTOM_ALT_B")]
    CustomAltB,
    #[token_de(token = "CUSTOM_ALT_C")]
    CustomAltC,
    #[token_de(token = "CUSTOM_ALT_D")]
    CustomAltD,
    #[token_de(token = "CUSTOM_ALT_E")]
    CustomAltE,
    #[token_de(token = "CUSTOM_ALT_F")]
    CustomAltF,
    #[token_de(token = "CUSTOM_ALT_G")]
    CustomAltG,
    #[token_de(token = "CUSTOM_ALT_H")]
    CustomAltH,
    #[token_de(token = "CUSTOM_ALT_I")]
    CustomAltI,
    #[token_de(token = "CUSTOM_ALT_J")]
    CustomAltJ,
    #[token_de(token = "CUSTOM_ALT_K")]
    CustomAltK,
    #[token_de(token = "CUSTOM_ALT_L")]
    CustomAltL,
    #[token_de(token = "CUSTOM_ALT_M")]
    CustomAltM,
    #[token_de(token = "CUSTOM_ALT_N")]
    CustomAltN,
    #[token_de(token = "CUSTOM_ALT_O")]
    CustomAltO,
    #[token_de(token = "CUSTOM_ALT_P")]
    CustomAltP,
    #[token_de(token = "CUSTOM_ALT_Q")]
    CustomAltQ,
    #[token_de(token = "CUSTOM_ALT_R")]
    CustomAltR,
    #[token_de(token = "CUSTOM_ALT_S")]
    CustomAltS,
    #[token_de(token = "CUSTOM_ALT_T")]
    CustomAltT,
    #[token_de(token = "CUSTOM_ALT_U")]
    CustomAltU,
    #[token_de(token = "CUSTOM_ALT_V")]
    CustomAltV,
    #[token_de(token = "CUSTOM_ALT_W")]
    CustomAltW,
    #[token_de(token = "CUSTOM_ALT_X")]
    CustomAltX,
    #[token_de(token = "CUSTOM_ALT_Y")]
    CustomAltY,
    #[token_de(token = "CUSTOM_ALT_Z")]
    CustomAltZ,
}

impl Default for KeyBindEnum {
    fn default() -> Self {
        Self::CustomA
    }
}
