use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
/// Plural shorthand alternatives
pub enum PluralEnum {
    /// No Plural
    #[token_de(token = "NP")]
    Np,
    /// Standard Plural, adds an 's' on the end
    #[token_de(token = "STP")]
    Stp,
}
impl Default for PluralEnum {
    fn default() -> Self {
        Self::Np
    }
}
