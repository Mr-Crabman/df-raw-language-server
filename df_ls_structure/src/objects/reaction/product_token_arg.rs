use super::{ItemReferenceArg, MaterialFromReagent, MaterialReferenceArg, NoSubtypeEnum};
use crate::NoneEnum;
use df_ls_core::{Choose, Reference};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_syntax_analysis::TokenDeserialize;
use df_ls_syntax_analysis::{LoopControl, Token, TokenValue, TreeCursor, TryFromArgument};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct ProductTokenArg {
    // Tarn comment:
    // > In the product, if you want to use the reagent's material itself,
    // > use NONE instead of a reaction product class (TAN_MAT in this example).
    // |---0---|-1-|2|-----------------3--------------------|---------------------4-------------------|
    // [PRODUCT:100:1:SKIN_TANNED:NONE                      :GET_MATERIAL_FROM_REAGENT:A     :TAN_MAT ]
    // [PRODUCT:100:1:BAR        :NONE                      :GET_MATERIAL_FROM_REAGENT:tallow:SOAP_MAT]
    // [PRODUCT:100:4:BAR        :NO_SUBTYPE                :METAL                    :STERLING_SILVER]
    // [PRODUCT:100:1:WEAPON     :ITEM_WEAPON_SPEAR_TRAINING:GET_MATERIAL_FROM_REAGENT:log   :NONE    ]
    /// Argument group 1
    pub probability_success: u8,
    /// Argument group 2
    pub quantity: u32,
    /// Argument group 3
    pub item_token: ItemReferenceArg,
    /// Argument group 4
    // https://dwarffortresswiki.org/index.php/Material_token
    pub material_token: Choose<MaterialReferenceArg, MaterialFromReagent>,
}

impl TokenDeserialize for ProductTokenArg {
    #[allow(clippy::field_reassign_with_default)]
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        token.check_token_arg0(&source, &mut diagnostics, true)?;
        token.check_arguments_length_fixed((6, 8), &source, &mut diagnostics, true)?;

        let mut result = Self::default();
        // Argument group 1
        result.probability_success =
            u8::try_from_argument(token.arguments.get(1), source, &mut diagnostics, true)?;
        // Argument group 2
        result.quantity =
            u32::try_from_argument(token.arguments.get(2), source, &mut diagnostics, true)?;

        // Argument group 3
        result.item_token = ItemReferenceArg {
            item_type: Reference::try_from_argument(
                token.arguments.get(3),
                source,
                &mut diagnostics,
                true,
            )?,
            item_subtype: Choose::<NoSubtypeEnum, Reference>::try_from_argument(
                token.arguments.get(4),
                source,
                &mut diagnostics,
                true,
            )?,
        };

        // Argument group 4
        let arg5 = token.get_argument(5)?;
        if let TokenValue::TVReference(v) = &arg5.value {
            match v.as_ref() {
                "GET_MATERIAL_FROM_REAGENT" => {
                    // recheck length
                    token.check_arguments_length_fixed((7, 7), &source, &mut diagnostics, true)?;
                    let reagent_reference = Reference::try_from_argument(
                        token.arguments.get(6),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    let reagent_material = Choose::<NoneEnum, Reference>::try_from_argument(
                        token.arguments.get(7),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    result.material_token = Choose::Choice2(MaterialFromReagent {
                        reagent_reference,
                        reagent_material,
                    });
                }
                _ => {
                    // recheck length
                    token.check_arguments_length_fixed((6, 6), &source, &mut diagnostics, true)?;
                    let material_type = Reference::try_from_argument(
                        token.arguments.get(5),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    let material_subtype = Choose::<NoSubtypeEnum, Reference>::try_from_argument(
                        token.arguments.get(6),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    result.material_token = Choose::Choice1(MaterialReferenceArg {
                        material_type,
                        material_subtype,
                    });
                }
            }
        } else {
            diagnostics.add_message(
                DMExtraInfo {
                    range: arg5.node.get_range(),
                    message_template_data: hash_map! {
                        "expected_parameters" => Reference::expected_argument_types(),
                        "found_parameters" => TokenValue::argument_to_token_name(&arg5.value),
                    },
                },
                "wrong_arg_type",
            );
        }
        Ok(result)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}
