use super::{MaterialFromReagent, MaterialReferenceArg, NoSubtypeEnum};
use crate::NoneEnum;
use df_ls_core::{Choose, Reference};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_syntax_analysis::TokenDeserialize;
use df_ls_syntax_analysis::{LoopControl, Token, TokenValue, TreeCursor, TryFromArgument};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct ImprovementTokenArg {
    // |-----0-----|-1-|----2-----|----------3----------|---------------------4--------------------|
    // [IMPROVEMENT:100:instrument:INSTRUMENT_PIECE:BODY:GET_MATERIAL_FROM_REAGENT:drum  :NONE     ]
    // [IMPROVEMENT:100:jug       :GLAZED               :GET_MATERIAL_FROM_REAGENT:glaze :GLAZE_MAT]
    // [IMPROVEMENT:100:a         :INSTRUMENT_PIECE:BODY:METAL                    :FRAME           ]
    // [IMPROVEMENT:100:target    :SPIKES               :GET_MATERIAL_FROM_REAGENT:gem   :NONE     ]
    /// Argument group 1
    pub chance: u8,
    /// Argument group 2
    pub reagent: Reference, // ReferenceTo<ReagentToken>
    /// Argument group 3
    pub improvement_type: Choose<ImprovementType, Reference>, // choose2: ReferenceTo<ToolToken>
    /// Argument group 4
    // https://dwarffortresswiki.org/index.php/Material_token
    pub material_token: Choose<MaterialReferenceArg, MaterialFromReagent>,
}

impl TokenDeserialize for ImprovementTokenArg {
    #[allow(clippy::field_reassign_with_default)]
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        token.check_token_arg0(&source, &mut diagnostics, true)?;
        token.check_arguments_length_fixed((6, 8), &source, &mut diagnostics, true)?;

        let mut result = Self::default();
        // Argument group 1
        result.chance =
            u8::try_from_argument(token.arguments.get(1), source, &mut diagnostics, true)?;
        // Argument group 2
        result.reagent =
            Reference::try_from_argument(token.arguments.get(2), source, &mut diagnostics, true)?;

        // Argument group 3
        let mut argument_count = 3;
        let arg3 = token.get_argument(argument_count)?;
        if let TokenValue::TVReference(v) = &arg3.value {
            match v.as_ref() {
                "INSTRUMENT_PIECE" => {
                    argument_count += 1;
                    result.improvement_type = Choose::Choice2(Reference::try_from_argument(
                        token.arguments.get(argument_count),
                        source,
                        &mut diagnostics,
                        true,
                    )?);
                }
                _ => {
                    result.improvement_type = Choose::Choice1(ImprovementType::try_from_argument(
                        token.arguments.get(argument_count),
                        source,
                        &mut diagnostics,
                        true,
                    )?);
                }
            }
        } else {
            diagnostics.add_message(
                DMExtraInfo {
                    range: arg3.node.get_range(),
                    message_template_data: hash_map! {
                        "expected_parameters" => Reference::expected_argument_types(),
                        "found_parameters" => TokenValue::argument_to_token_name(&arg3.value),
                    },
                },
                "wrong_arg_type",
            );
        }

        // Argument group 4
        argument_count += 1;
        let arg4 = token.get_argument(argument_count)?;
        if let TokenValue::TVReference(v) = &arg4.value {
            match v.as_ref() {
                "GET_MATERIAL_FROM_REAGENT" => {
                    // recheck length
                    token.check_arguments_length_fixed(
                        (argument_count as u32 + 2, argument_count as u32 + 2),
                        &source,
                        &mut diagnostics,
                        true,
                    )?;
                    argument_count += 1;
                    let reagent_reference = Reference::try_from_argument(
                        token.arguments.get(argument_count),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    argument_count += 1;
                    let reagent_material = Choose::<NoneEnum, Reference>::try_from_argument(
                        token.arguments.get(argument_count),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    result.material_token = Choose::Choice2(MaterialFromReagent {
                        reagent_reference,
                        reagent_material,
                    });
                }
                _ => {
                    // recheck length
                    token.check_arguments_length_fixed(
                        (argument_count as u32 + 1, argument_count as u32 + 1),
                        &source,
                        &mut diagnostics,
                        true,
                    )?;
                    let material_type = Reference::try_from_argument(
                        token.arguments.get(argument_count),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    argument_count += 1;
                    let material_subtype = Choose::<NoSubtypeEnum, Reference>::try_from_argument(
                        token.arguments.get(argument_count),
                        source,
                        &mut diagnostics,
                        true,
                    )?;
                    result.material_token = Choose::Choice1(MaterialReferenceArg {
                        material_type,
                        material_subtype,
                    });
                }
            }
        } else {
            diagnostics.add_message(
                DMExtraInfo {
                    range: arg4.node.get_range(),
                    message_template_data: hash_map! {
                        "expected_parameters" => Reference::expected_argument_types(),
                        "found_parameters" => TokenValue::argument_to_token_name(&arg4.value),
                    },
                },
                "wrong_arg_type",
            );
        }
        Ok(result)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum ImprovementType {
    #[token_de(token = "COVERED")]
    Covered,
    #[token_de(token = "GLAZED")]
    Glazed,
    #[token_de(token = "BANDS")]
    Bands,
    #[token_de(token = "RINGS_HANGING")]
    RingsHanging,
    #[token_de(token = "SPIKES")]
    Spikes,
    #[token_de(token = "INSTRUMENT_PIECE")]
    InstrumentPiece,
}

impl Default for ImprovementType {
    fn default() -> Self {
        Self::Covered
    }
}
