use df_ls_core::{Reference, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct CreatureToken {
    /// argument 1 of `CREATURE`
    #[token_de(token = "CREATURE", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Nested token `CASTE`
    #[token_de(token = "CASTE")]
    pub castes: Vec<Caste>,
    /// Nested token `SELECT_CASTE`
    #[token_de(token = "SELECT_CASTE")]
    pub select_castes: Vec<SelectCaste>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct Caste {
    /// argument 1 of `CASTE`
    #[token_de(token = "CASTE", on_duplicate_to_parent, primary_token)]
    pub reference: Option<Reference>,
    /// Nested token `FEMALE`
    #[token_de(token = "FEMALE")]
    pub female: Option<()>,
    /// Nested token `MALE`
    #[token_de(token = "MALE")]
    pub male: Option<()>,
    /// Nested token `DESCRIPTION`
    #[token_de(token = "DESCRIPTION")]
    pub description: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct SelectCaste {
    /// argument 1 of `SELECT_CASTE`
    #[token_de(token = "SELECT_CASTE", on_duplicate_to_parent, primary_token)]
    pub reference: Option<Reference>,
    /// Nested token `FEMALE`
    #[token_de(token = "FEMALE")]
    pub female: Option<()>,
    /// Nested token `MALE`
    #[token_de(token = "MALE")]
    pub male: Option<()>,
    /// Nested token `DESCRIPTION`
    #[token_de(token = "DESCRIPTION")]
    pub description: Option<String>,
    /// Nested token `NATURAL`
    #[token_de(token = "NATURAL")]
    pub natural: Option<()>,
}
