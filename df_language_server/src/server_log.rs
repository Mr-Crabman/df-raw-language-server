use colored::*;
use log::{Level, Metadata, Record};

/// An instance of the `Logger`.
pub static LOGGER: Logger = Logger;
/// The log collector and handler for most printed messages in terminal.
pub struct Logger;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        let enable = if !cfg!(debug_assertions) {
            // Only in release mode
            // Do the filters below unless it is a Warning, Error (or Debug)
            metadata.level() == Level::Warn
                || metadata.level() == Level::Error
                || metadata.level() == Level::Debug
        } else {
            // Don't apply additional filters in debug build
            true
        };

        // All messages need to be Trace or lower
        metadata.level() <= Level::Trace
            // Don't display `ureq` message (can be enabled for testing)
            && metadata.target() != "ureq::unit"
            // Don't display hyper networking message (not useful in most cases)
            && !metadata.target().starts_with("hyper::")
            // If release mode filter on
            && enable
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{:<5}:{} - {}",
                match record.level() {
                    Level::Error => "ERROR".bright_red(),
                    Level::Warn => "WARN".bright_yellow(),
                    Level::Info => "INFO".bright_blue(),
                    Level::Debug => "DEBUG".bright_green(),
                    Level::Trace => "TRACE".bright_magenta(),
                },
                record.target(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}
