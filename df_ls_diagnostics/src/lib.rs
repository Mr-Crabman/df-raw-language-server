#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod diagnostics_info;

pub use diagnostics_info::{
    DMExtraInfo, DiagnosticMessageSet, DiagnosticsInfo, DiagnosticsMessage, Position, Range,
};
pub use lsp_types;

use rust_embed::*;

#[derive(RustEmbed)]
#[folder = "./diagnostics_messages/"]
pub(crate) struct DiagnosticsMessages;
