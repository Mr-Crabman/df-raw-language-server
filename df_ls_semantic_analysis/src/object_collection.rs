use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_structure::*;
use std::collections::HashMap;

#[derive(Default, PartialEq)]
pub struct ObjectCollection {
    pub body_tokens: HashMap<ReferenceTo<BodyToken>, BodyToken>,
    // TODO: need a solution for storing this
    // pub bodygloss_tokens: HashMap<ReferenceTo<BodyGlossToken>, BodyGlossToken>,
    pub body_detail_plan_tokens: HashMap<ReferenceTo<BodyDetailPlanToken>, BodyDetailPlanToken>,
    pub building_tokens: HashMap<ReferenceTo<BuildingToken>, BuildingToken>,
    pub creature_tokens: HashMap<ReferenceTo<CreatureToken>, CreatureToken>,
    pub creature_variation_tokens:
        HashMap<ReferenceTo<CreatureVariationToken>, CreatureVariationToken>,
    pub color_tokens: HashMap<ReferenceTo<ColorToken>, ColorToken>,
    pub pattern_tokens: HashMap<ReferenceTo<PatternToken>, PatternToken>,
    pub shape_tokens: HashMap<ReferenceTo<ShapeToken>, ShapeToken>,
    pub entity_tokens: HashMap<ReferenceTo<EntityToken>, EntityToken>,
    pub graphics_tokens: HashMap<ReferenceTo<GraphicsToken>, GraphicsToken>,
    pub interaction_tokens: HashMap<ReferenceTo<InteractionToken>, InteractionToken>,
    pub inorganic_tokens: HashMap<ReferenceTo<InorganicToken>, InorganicToken>,
    pub material_tokens: HashMap<ReferenceTo<MaterialToken>, MaterialToken>,
    pub plant_tokens: HashMap<ReferenceTo<PlantToken>, PlantToken>,
    pub reaction_tokens: HashMap<ReferenceTo<ReactionToken>, ReactionToken>,
    pub tissue_template_tokens: HashMap<ReferenceTo<TissueToken>, TissueToken>,
    // Item objects
    pub ammo_tokens: HashMap<ReferenceTo<AmmoToken>, AmmoToken>,
    pub armor_tokens: HashMap<ReferenceTo<ArmorToken>, ArmorToken>,
    pub food_tokens: HashMap<ReferenceTo<FoodToken>, FoodToken>,
    pub gloves_tokens: HashMap<ReferenceTo<GlovesToken>, GlovesToken>,
    pub helm_tokens: HashMap<ReferenceTo<HelmToken>, HelmToken>,
    pub instrument_tokens: HashMap<ReferenceTo<InstrumentToken>, InstrumentToken>,
    pub pants_tokens: HashMap<ReferenceTo<PantsToken>, PantsToken>,
    pub shield_tokens: HashMap<ReferenceTo<ShieldToken>, ShieldToken>,
    pub shoes_tokens: HashMap<ReferenceTo<ShoesToken>, ShoesToken>,
    pub siege_ammo_tokens: HashMap<ReferenceTo<SiegeAmmoToken>, SiegeAmmoToken>,
    pub tool_tokens: HashMap<ReferenceTo<ToolToken>, ToolToken>,
    pub toy_tokens: HashMap<ReferenceTo<ToyToken>, ToyToken>,
    pub trap_comp_tokens: HashMap<ReferenceTo<TrapCompToken>, TrapCompToken>,
    pub weapon_tokens: HashMap<ReferenceTo<WeaponToken>, WeaponToken>,
    // Language objects
    pub word_tokens: HashMap<ReferenceTo<WordToken>, WordToken>,
    pub symbol_tokens: HashMap<ReferenceTo<SymbolToken>, SymbolToken>,
    pub translation_tokens: HashMap<ReferenceTo<TranslationToken>, TranslationToken>,
}

/// Macro for adding all objects in a certain category to their respective HashMap.
macro_rules! add_objects {
    ($object_hashmap:expr, $object_tokens:expr, $diagnostics:expr) => {
        for object in $object_tokens {
            match &object.get_reference() {
                Some(key_ref) => {
                    if $object_hashmap.contains_key(key_ref) {
                        add_duplicate_object_id_diagnostic(key_ref, $diagnostics);
                    } else {
                        $object_hashmap.insert(key_ref.clone(), object.clone());
                    }
                }
                None => { /* Error already handled elsewhere */ }
            }
        }
    };
}

/// Macro for adding all objects in a certain category to their respective HashMap,
/// but specific to the "enum" object types, `ItemToken` and `LanguageToken`.
macro_rules! add_enum_objects {
    ($object_hashmap:expr, $object:expr, $diagnostics:expr) => {
        match &$object.reference {
            Some(key_ref) => {
                if $object_hashmap.contains_key(key_ref) {
                    add_duplicate_object_id_diagnostic(key_ref, $diagnostics);
                } else {
                    $object_hashmap.insert(key_ref.clone(), $object.clone());
                }
            }
            None => { /* Error already handled elsewhere */ }
        }
    };
}

/// Returns a collection of all objects/references in the file.
pub fn create_object_collection(
    structure: &DFRaw,
    diagnostics: &mut DiagnosticsInfo,
) -> ObjectCollection {
    let mut woc = ObjectCollection {
        ..Default::default()
    };
    for object_token in &structure.object_tokens {
        add_objects!(
            woc.body_detail_plan_tokens,
            &object_token.body_detail_plan_tokens,
            diagnostics
        );
        add_objects!(
            woc.building_tokens,
            &object_token.building_tokens,
            diagnostics
        );
        add_objects!(
            woc.creature_tokens,
            &object_token.creature_tokens,
            diagnostics
        );
        add_objects!(
            woc.creature_variation_tokens,
            &object_token.creature_variation_tokens,
            diagnostics
        );
        add_objects!(woc.color_tokens, &object_token.color_tokens, diagnostics);
        add_objects!(
            woc.pattern_tokens,
            &object_token.pattern_tokens,
            diagnostics
        );
        add_objects!(woc.shape_tokens, &object_token.shape_tokens, diagnostics);
        add_objects!(woc.entity_tokens, &object_token.entity_tokens, diagnostics);
        add_objects!(
            woc.graphics_tokens,
            &object_token.graphics_tokens,
            diagnostics
        );
        add_objects!(
            woc.interaction_tokens,
            &object_token.interaction_tokens,
            diagnostics
        );
        add_objects!(
            woc.inorganic_tokens,
            &object_token.inorganic_tokens,
            diagnostics
        );
        add_objects!(
            woc.material_tokens,
            &object_token.material_tokens,
            diagnostics
        );
        add_objects!(woc.plant_tokens, &object_token.plant_tokens, diagnostics);
        add_objects!(
            woc.reaction_tokens,
            &object_token.reaction_tokens,
            diagnostics
        );
        add_objects!(
            woc.tissue_template_tokens,
            &object_token.tissue_template_tokens,
            diagnostics
        );
        // Custom logic is needed to handle these tokens, which use an enum
        for body_token in &object_token.body_tokens {
            match body_token {
                BodyObjectToken::BodyToken(body) => {
                    add_enum_objects!(woc.body_tokens, body, diagnostics)
                }
                BodyObjectToken::BodyGlossToken(_bodygloss) => {
                    // TODO: need a solution for storing this
                    // add_enum_objects!(woc.bodygloss_tokens, body, diagnostics)
                }
            }
        }
        for item_token in &object_token.item_tokens {
            match item_token {
                ItemToken::AmmoToken(item) => {
                    add_enum_objects!(woc.ammo_tokens, item, diagnostics)
                }
                ItemToken::ArmorToken(item) => {
                    add_enum_objects!(woc.armor_tokens, item, diagnostics)
                }
                ItemToken::FoodToken(item) => {
                    add_enum_objects!(woc.food_tokens, item, diagnostics)
                }
                ItemToken::GlovesToken(item) => {
                    add_enum_objects!(woc.gloves_tokens, item, diagnostics)
                }
                ItemToken::HelmToken(item) => {
                    add_enum_objects!(woc.helm_tokens, item, diagnostics)
                }
                ItemToken::InstrumentToken(item) => {
                    add_enum_objects!(woc.instrument_tokens, item, diagnostics)
                }
                ItemToken::PantsToken(item) => {
                    add_enum_objects!(woc.pants_tokens, item, diagnostics)
                }
                ItemToken::ShieldToken(item) => {
                    add_enum_objects!(woc.shield_tokens, item, diagnostics)
                }
                ItemToken::ShoesToken(item) => {
                    add_enum_objects!(woc.shoes_tokens, item, diagnostics)
                }
                ItemToken::SiegeAmmoToken(item) => {
                    add_enum_objects!(woc.siege_ammo_tokens, item, diagnostics)
                }
                ItemToken::ToolToken(item) => {
                    add_enum_objects!(woc.tool_tokens, item, diagnostics)
                }
                ItemToken::ToyToken(item) => {
                    add_enum_objects!(woc.toy_tokens, item, diagnostics)
                }
                ItemToken::TrapCompToken(item) => {
                    add_enum_objects!(woc.trap_comp_tokens, item, diagnostics)
                }
                ItemToken::WeaponToken(item) => {
                    add_enum_objects!(woc.weapon_tokens, item, diagnostics)
                }
            }
        }
        for language_token in &object_token.language_tokens {
            match language_token {
                LanguageToken::WordToken(word) => {
                    add_enum_objects!(woc.word_tokens, word, diagnostics)
                }
                LanguageToken::SymbolToken(symbol) => {
                    add_enum_objects!(woc.symbol_tokens, symbol, diagnostics)
                }
                LanguageToken::TranslationToken(translation) => {
                    add_enum_objects!(woc.translation_tokens, translation, diagnostics)
                }
            }
        }
    }
    woc
}

/// Add a `duplicate_object_id` diagnostic
fn add_duplicate_object_id_diagnostic<T: Referenceable>(
    object_id: &ReferenceTo<T>,
    _diagnostics: &mut DiagnosticsInfo,
) {
    // TODO when ranges are added, use them to mark this
    // diagnostics.add_message(
    //     DMExtraInfo {
    //         node: &node,
    //         expected_tokens: None,
    //         expected_parameters: None,
    //         found_parameters: None,
    //         found_token: Some(DMExtraInfo::get_node_text(&node, source)),
    //         error: None,
    //     },
    //     "duplicate_token_id",
    // );
    println!("Found a duplicate {:?} object", object_id);
}
