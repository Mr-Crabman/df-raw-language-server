# Roadmap

These are the LSP features that are planned to be implemented; listed *roughly* in order of priority (but not strictly).

## WIP
- [ ] Errors and warnings/diagnostics for lexical, syntax, and semantic analysis.
- [ ] Implement all token types (listed in `README.md`).

## Future
- [ ] Checking all files in workspace.
- [ ] Quick fix/code action for some diagnostics where possible.
---
- [ ] Hover token description.
- [ ] Hover signature argument types/names.
---
- [ ] Autocomplete allowed tokens and enums (have to be careful when multiple types are allowed, eg integer or `NONE` enum).
---
- [ ] Jump to definition of object, caste, or other user definable thing referenced in multiple places.
- [ ] Find references of object, caste, or other user definable thing referenced in multiple places.
---
- [ ] Refactoring for object, caste, or other user definable thing referenced in multiple places.
- [ ] Format/beautify code: apply indentation according to nesting.
---
**Semantic highlighting:**
- [ ] Coloring enums differently to references.
- [ ] Coloring token names properly in tokens like `GO_TO_TAG`, which use the names of other tokens as arguments.
- [ ] Coloring "ref" types that are neither object references or enums (eg, the argument in `SELECT_CASTE`).
- [ ] Coloring `OBJECT` and object type tokens (eg `CREATURE`, `ENTITY`) differently to regular tokens (and maybe each other).
---
### Stretch goals

- [ ] Support code folding based on logical nesting (rather than current indentation, which might be wrong).
- [ ] Support code region comments (for code folding) using special starting character(s) followed by `region`.
- [ ] Support "docstring" comments for objects, using special starting comment character(s) just before an object definition.
---
- [ ] Format code: sorting tokens (within a nesting level; the sorting must not affect functionality) and putting short, related tokens on the same line. Must be a separate option to other formatting, and triggered manually.